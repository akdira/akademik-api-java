package id.code.akademik;

import id.code.server.ApiServerApplication;

import id.code.akademik.api.Route;
import id.code.akademik.api.middleware.AccessTokenMiddleware;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class Application {
    public static void main(String[] args) {
        createApplication("akademik.properties").standaloneStart();
    }

    static ApiServerApplication createApplication(String configPath) {
        return new ApiServerApplication()
            .setPropertiesPath(configPath)
            .setRoutes(Route::createRoutes)
            .setAccessTokenMiddleware(AccessTokenMiddleware::new);
    }
}