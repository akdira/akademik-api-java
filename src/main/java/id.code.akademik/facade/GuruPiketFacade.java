package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.GuruPiketModel;
import id.code.akademik.filter.GuruPiketFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class GuruPiketFacade {
    public boolean insert(GuruPiketModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(GuruPiketModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(int kdPiket) throws SQLException, QueryBuilderException, IOException {
        final GuruPiketModel oldData = this.findByKdPiket(kdPiket);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<GuruPiketModel> getAll(Filter<GuruPiketFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(GuruPiketModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(GuruPiketModel.class);
    }

    public GuruPiketModel findByKdPiket(int kdPiket) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(GuruPiketModel.class)
                .where(GuruPiketModel._KD_PIKET).isEqual(kdPiket)
                .getResult(DatabasePool.getConnection())
                .executeItem(GuruPiketModel.class);
    }
}