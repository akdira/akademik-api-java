package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.AbsensiHarianDetailModel;
import id.code.akademik.filter.AbsensiHarianDetailFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class AbsensiHarianDetailFacade {
    public boolean insert(AbsensiHarianDetailModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(AbsensiHarianDetailModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(int kdAbsenDetail) throws SQLException, QueryBuilderException, IOException {
        final AbsensiHarianDetailModel oldData = this.findByKdAbsenDetail(kdAbsenDetail);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<AbsensiHarianDetailModel> getAll(Filter<AbsensiHarianDetailFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(AbsensiHarianDetailModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(AbsensiHarianDetailModel.class);
    }

    public AbsensiHarianDetailModel findByKdAbsenDetail(int kdAbsenDetail) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(AbsensiHarianDetailModel.class)
                .where(AbsensiHarianDetailModel._KD_ABSEN_DETAIL).isEqual(kdAbsenDetail)
                .getResult(DatabasePool.getConnection())
                .executeItem(AbsensiHarianDetailModel.class);
    }
}