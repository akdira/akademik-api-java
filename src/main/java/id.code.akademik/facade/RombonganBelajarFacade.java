package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.RombonganBelajarModel;
import id.code.akademik.filter.RombonganBelajarFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class RombonganBelajarFacade {
    public boolean insert(RombonganBelajarModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(RombonganBelajarModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long kdRombel) throws SQLException, QueryBuilderException, IOException {
        final RombonganBelajarModel oldData = this.findByKdRombel(kdRombel);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<RombonganBelajarModel> getAll(Filter<RombonganBelajarFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(RombonganBelajarModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(RombonganBelajarModel.class);
    }

    public RombonganBelajarModel findByKdRombel(long kdRombel) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(RombonganBelajarModel.class)
                .where(RombonganBelajarModel._KD_ROMBEL).isEqual(kdRombel)
                .getResult(DatabasePool.getConnection())
                .executeItem(RombonganBelajarModel.class);
    }
}