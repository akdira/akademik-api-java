package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.WaliKelasModel;
import id.code.akademik.filter.WaliKelasFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class WaliKelasFacade {
    public boolean insert(WaliKelasModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(WaliKelasModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long kdWk) throws SQLException, QueryBuilderException, IOException {
        final WaliKelasModel oldData = this.findByKdWk(kdWk);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<WaliKelasModel> getAll(Filter<WaliKelasFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(WaliKelasModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(WaliKelasModel.class);
    }

    public WaliKelasModel findByKdWk(long kdWk) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(WaliKelasModel.class)
                .where(WaliKelasModel._KD_WK).isEqual(kdWk)
                .getResult(DatabasePool.getConnection())
                .executeItem(WaliKelasModel.class);
    }
}