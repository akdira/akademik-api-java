package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.KelasModel;
import id.code.akademik.filter.KelasFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class KelasFacade {
    public boolean insert(KelasModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(KelasModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(String kdKls) throws SQLException, QueryBuilderException, IOException {
        final KelasModel oldData = this.findByKdKls(kdKls);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<KelasModel> getAll(Filter<KelasFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(KelasModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(KelasModel.class);
    }

    public KelasModel findByKdKls(String kdKls) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(KelasModel.class)
                .where(KelasModel._KD_KLS).isEqual(kdKls)
                .getResult(DatabasePool.getConnection())
                .executeItem(KelasModel.class);
    }
}