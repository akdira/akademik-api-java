package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.SiswaModel;
import id.code.akademik.filter.SiswaFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class SiswaFacade {
    public boolean insert(SiswaModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(SiswaModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(String nis) throws SQLException, QueryBuilderException, IOException {
        final SiswaModel oldData = this.findByNis(nis);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<SiswaModel> getAll(Filter<SiswaFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(SiswaModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .orderBy("DATE_CREATE").desc()
                .orderBy("DATE_UPDATE").desc()
                .getResult(DatabasePool.getConnection())
                .executeItems(SiswaModel.class);
    }

    public SiswaModel findByNis(String nis) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(SiswaModel.class)
                .where(SiswaModel._NIS).isEqual(nis)
                .getResult(DatabasePool.getConnection())
                .executeItem(SiswaModel.class);
    }
}