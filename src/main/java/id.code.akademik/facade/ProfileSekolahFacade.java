package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.ProfileSekolahModel;
import id.code.akademik.filter.ProfileSekolahFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class ProfileSekolahFacade {
    public boolean insert(ProfileSekolahModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(ProfileSekolahModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(int idSekolah) throws SQLException, QueryBuilderException, IOException {
        final ProfileSekolahModel oldData = this.findByIdSekolah(idSekolah);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<ProfileSekolahModel> getAll(Filter<ProfileSekolahFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(ProfileSekolahModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(ProfileSekolahModel.class);
    }

    public ProfileSekolahModel findByIdSekolah(int idSekolah) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(ProfileSekolahModel.class)
                .where(ProfileSekolahModel._ID_SEKOLAH).isEqual(idSekolah)
                .getResult(DatabasePool.getConnection())
                .executeItem(ProfileSekolahModel.class);
    }
}