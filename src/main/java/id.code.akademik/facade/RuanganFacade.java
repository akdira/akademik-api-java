package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.RuanganModel;
import id.code.akademik.filter.RuanganFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class RuanganFacade {
    public boolean insert(RuanganModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(RuanganModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(String kdRuang) throws SQLException, QueryBuilderException, IOException {
        final RuanganModel oldData = this.findByKdRuang(kdRuang);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<RuanganModel> getAll(Filter<RuanganFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(RuanganModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(RuanganModel.class);
    }

    public RuanganModel findByKdRuang(String kdRuang) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(RuanganModel.class)
                .where(RuanganModel._KD_RUANG).isEqual(kdRuang)
                .getResult(DatabasePool.getConnection())
                .executeItem(RuanganModel.class);
    }
}