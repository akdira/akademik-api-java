package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.HariModel;
import id.code.akademik.filter.HariFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class HariFacade {
    public boolean insert(HariModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(HariModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(int kdHari) throws SQLException, QueryBuilderException, IOException {
        final HariModel oldData = this.findByKdHari(kdHari);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<HariModel> getAll(Filter<HariFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(HariModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(HariModel.class);
    }

    public HariModel findByKdHari(int kdHari) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(HariModel.class)
                .where(HariModel._KD_HARI).isEqual(kdHari)
                .getResult(DatabasePool.getConnection())
                .executeItem(HariModel.class);
    }
}