package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.AbsensiHarianModel;
import id.code.akademik.filter.AbsensiHarianFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class AbsensiHarianFacade {
    public boolean insert(AbsensiHarianModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(AbsensiHarianModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(int kdAbsen) throws SQLException, QueryBuilderException, IOException {
        final AbsensiHarianModel oldData = this.findByKdAbsen(kdAbsen);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<AbsensiHarianModel> getAll(Filter<AbsensiHarianFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(AbsensiHarianModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(AbsensiHarianModel.class);
    }

    public AbsensiHarianModel findByKdAbsen(int kdAbsen) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(AbsensiHarianModel.class)
                .where(AbsensiHarianModel._KD_ABSEN).isEqual(kdAbsen)
                .getResult(DatabasePool.getConnection())
                .executeItem(AbsensiHarianModel.class);
    }
}