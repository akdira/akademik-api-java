package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.SubKelompokModel;
import id.code.akademik.filter.SubKelompokFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class SubKelompokFacade {
    public boolean insert(SubKelompokModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(SubKelompokModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(int kdSubkel) throws SQLException, QueryBuilderException, IOException {
        final SubKelompokModel oldData = this.findByKdSubkel(kdSubkel);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<SubKelompokModel> getAll(Filter<SubKelompokFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(SubKelompokModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(SubKelompokModel.class);
    }

    public SubKelompokModel findByKdSubkel(int kdSubkel) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(SubKelompokModel.class)
                .where(SubKelompokModel._KD_SUBKEL).isEqual(kdSubkel)
                .getResult(DatabasePool.getConnection())
                .executeItem(SubKelompokModel.class);
    }
}