package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.HakAksesModel;
import id.code.akademik.filter.HakAksesFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class HakAksesFacade {
    public boolean insert(HakAksesModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(HakAksesModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(int idAkses) throws SQLException, QueryBuilderException, IOException {
        final HakAksesModel oldData = this.findByIdAkses(idAkses);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<HakAksesModel> getAll(Filter<HakAksesFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(HakAksesModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(HakAksesModel.class);
    }

    public HakAksesModel findByIdAkses(int idAkses) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(HakAksesModel.class)
                .where(HakAksesModel._ID_AKSES).isEqual(idAkses)
                .getResult(DatabasePool.getConnection())
                .executeItem(HakAksesModel.class);
    }
}