package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.JabatanModel;
import id.code.akademik.filter.JabatanFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class JabatanFacade {
    public boolean insert(JabatanModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(JabatanModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(int kdJab) throws SQLException, QueryBuilderException, IOException {
        final JabatanModel oldData = this.findByKdJab(kdJab);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<JabatanModel> getAll(Filter<JabatanFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(JabatanModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(JabatanModel.class);
    }

    public JabatanModel findByKdJab(int kdJab) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(JabatanModel.class)
                .where(JabatanModel._KD_JAB).isEqual(kdJab)
                .getResult(DatabasePool.getConnection())
                .executeItem(JabatanModel.class);
    }
}