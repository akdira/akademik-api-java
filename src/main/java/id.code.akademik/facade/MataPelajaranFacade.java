package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.MataPelajaranModel;
import id.code.akademik.filter.MataPelajaranFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class MataPelajaranFacade {
    public boolean insert(MataPelajaranModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(MataPelajaranModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(String kdPel) throws SQLException, QueryBuilderException, IOException {
        final MataPelajaranModel oldData = this.findByKdPel(kdPel);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<MataPelajaranModel> getAll(Filter<MataPelajaranFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(MataPelajaranModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(MataPelajaranModel.class);
    }

    public MataPelajaranModel findByKdPel(String kdPel) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(MataPelajaranModel.class)
                .where(MataPelajaranModel._KD_PEL).isEqual(kdPel)
                .getResult(DatabasePool.getConnection())
                .executeItem(MataPelajaranModel.class);
    }
}