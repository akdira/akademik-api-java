package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.JurnalModel;
import id.code.akademik.filter.JurnalFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class JurnalFacade {
    public boolean insert(JurnalModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(JurnalModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(int kdJurnal) throws SQLException, QueryBuilderException, IOException {
        final JurnalModel oldData = this.findByKdJurnal(kdJurnal);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<JurnalModel> getAll(Filter<JurnalFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(JurnalModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(JurnalModel.class);
    }

    public JurnalModel findByKdJurnal(int kdJurnal) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(JurnalModel.class)
                .where(JurnalModel._KD_JURNAL).isEqual(kdJurnal)
                .getResult(DatabasePool.getConnection())
                .executeItem(JurnalModel.class);
    }
}