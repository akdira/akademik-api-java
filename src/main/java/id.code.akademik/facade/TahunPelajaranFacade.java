package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.TahunPelajaranModel;
import id.code.akademik.filter.TahunPelajaranFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class TahunPelajaranFacade {
    public boolean insert(TahunPelajaranModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(TahunPelajaranModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(String kdThn) throws SQLException, QueryBuilderException, IOException {
        final TahunPelajaranModel oldData = this.findByKdThn(kdThn);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<TahunPelajaranModel> getAll(Filter<TahunPelajaranFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(TahunPelajaranModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(TahunPelajaranModel.class);
    }

    public TahunPelajaranModel findByKdThn(String kdThn) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(TahunPelajaranModel.class)
                .where(TahunPelajaranModel._KD_THN).isEqual(kdThn)
                .getResult(DatabasePool.getConnection())
                .executeItem(TahunPelajaranModel.class);
    }
}