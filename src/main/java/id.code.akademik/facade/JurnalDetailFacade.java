package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.JurnalDetailModel;
import id.code.akademik.filter.JurnalDetailFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class JurnalDetailFacade {
    public boolean insert(JurnalDetailModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(JurnalDetailModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(int kdJurnalDetail) throws SQLException, QueryBuilderException, IOException {
        final JurnalDetailModel oldData = this.findByKdJurnalDetail(kdJurnalDetail);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<JurnalDetailModel> getAll(Filter<JurnalDetailFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(JurnalDetailModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(JurnalDetailModel.class);
    }

    public JurnalDetailModel findByKdJurnalDetail(int kdJurnalDetail) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(JurnalDetailModel.class)
                .where(JurnalDetailModel._KD_JURNAL_DETAIL).isEqual(kdJurnalDetail)
                .getResult(DatabasePool.getConnection())
                .executeItem(JurnalDetailModel.class);
    }
}