package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.PenilaianDetailModel;
import id.code.akademik.filter.PenilaianDetailFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class PenilaianDetailFacade {
    public boolean insert(PenilaianDetailModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(PenilaianDetailModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(int kdNilaiDetail) throws SQLException, QueryBuilderException, IOException {
        final PenilaianDetailModel oldData = this.findByKdNilaiDetail(kdNilaiDetail);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<PenilaianDetailModel> getAll(Filter<PenilaianDetailFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(PenilaianDetailModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(PenilaianDetailModel.class);
    }

    public PenilaianDetailModel findByKdNilaiDetail(int kdNilaiDetail) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(PenilaianDetailModel.class)
                .where(PenilaianDetailModel._KD_NILAI_DETAIL).isEqual(kdNilaiDetail)
                .getResult(DatabasePool.getConnection())
                .executeItem(PenilaianDetailModel.class);
    }
}