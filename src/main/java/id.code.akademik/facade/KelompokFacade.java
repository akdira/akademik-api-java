package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.KelompokModel;
import id.code.akademik.filter.KelompokFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class KelompokFacade {
    public boolean insert(KelompokModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(KelompokModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(int kdKel) throws SQLException, QueryBuilderException, IOException {
        final KelompokModel oldData = this.findByKdKel(kdKel);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<KelompokModel> getAll(Filter<KelompokFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(KelompokModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(KelompokModel.class);
    }

    public KelompokModel findByKdKel(int kdKel) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(KelompokModel.class)
                .where(KelompokModel._KD_KEL).isEqual(kdKel)
                .getResult(DatabasePool.getConnection())
                .executeItem(KelompokModel.class);
    }
}