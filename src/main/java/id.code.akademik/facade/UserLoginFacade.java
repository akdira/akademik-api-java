package id.code.akademik.facade;

import com.google.common.base.Strings;
import id.code.component.PasswordHasher;
import id.code.component.DatabasePool;
import id.code.database.filter.Filter;
import id.code.database.builder.*;

import id.code.akademik.model.UserLoginModel;
import id.code.akademik.filter.UserLoginFilter;

import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class UserLoginFacade {
    public boolean insert(UserLoginModel value) throws SQLException, QueryBuilderException, IOException, GeneralSecurityException  {
        value.setPassword(PasswordHasher.computePasswordHash(value.getPassword()));
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(UserLoginModel new_data) throws SQLException, IOException, GeneralSecurityException {

        UserLoginModel old_data = this.findByIdUser(new_data.getIdUser());
        if(new_data.getPassword().length() == 0) {
            new_data.setPassword(old_data.getPassword());
        } else {
            new_data.setPassword(PasswordHasher.computePasswordHash(new_data.getPassword()));
        }

        final UpdateResult result = QueryBuilder.update(new_data).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(int idUser) throws SQLException, QueryBuilderException, IOException {
        final UserLoginModel oldData = this.findByIdUser(idUser);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<UserLoginModel> getAll(Filter<UserLoginFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(UserLoginModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(UserLoginModel.class);
    }

    public UserLoginModel findByIdUser(int idUser) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(UserLoginModel.class)
            .where(UserLoginModel._ID_USER).isEqual(idUser)
            .getResult(DatabasePool.getConnection())
            .executeItem(UserLoginModel.class);
    }

    public UserLoginModel login(String username, String password) throws SQLException, GeneralSecurityException, UnsupportedEncodingException, QueryBuilderException {
        final UserLoginModel model = QueryBuilder.select(UserLoginModel.class)
                .where(UserLoginModel._USERNAME).isEqual(username).limit(1)
                .getResult(DatabasePool.getConnection())
                .executeItem(UserLoginModel.class);

        return model != null && PasswordHasher.verify(password, model.getPassword()) ? model : null;
    }

    public boolean isUsernameAvailable(String username) throws SQLException, QueryBuilderException {
        try (ResultBuilder result = QueryBuilder.select(UserLoginModel.class, UserLoginModel._ID_USER)
                .where(UserLoginModel._USERNAME).isEqual(username).limit(1)
                .execute(DatabasePool.getConnection())) {
            return !result.moveNext();
        }
    }

    public boolean changePassword(UserLoginModel model, String newPassword) throws SQLException, GeneralSecurityException, UnsupportedEncodingException, QueryBuilderException {
        model.setPassword(PasswordHasher.computePasswordHash(newPassword));
        UpdateResult result = QueryBuilder.update(model, UserLoginModel._PASSWORD)
                .execute(DatabasePool.getConnection());
        return result.isModified();
    }
}