package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.JadwalMatapelModel;
import id.code.akademik.filter.JadwalMatapelFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class JadwalMatapelFacade {
    public boolean insert(JadwalMatapelModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(JadwalMatapelModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long kdJadwal) throws SQLException, QueryBuilderException, IOException {
        final JadwalMatapelModel oldData = this.findByKdJadwal(kdJadwal);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<JadwalMatapelModel> getAll(Filter<JadwalMatapelFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(JadwalMatapelModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(JadwalMatapelModel.class);
    }

    public JadwalMatapelModel findByKdJadwal(long kdJadwal) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(JadwalMatapelModel.class)
                .where(JadwalMatapelModel._KD_JADWAL).isEqual(kdJadwal)
                .getResult(DatabasePool.getConnection())
                .executeItem(JadwalMatapelModel.class);
    }
}