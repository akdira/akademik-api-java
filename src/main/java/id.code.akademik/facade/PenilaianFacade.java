package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.PenilaianModel;
import id.code.akademik.filter.PenilaianFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class PenilaianFacade {
    public boolean insert(PenilaianModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(PenilaianModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(int kdNilai) throws SQLException, QueryBuilderException, IOException {
        final PenilaianModel oldData = this.findByKdNilai(kdNilai);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<PenilaianModel> getAll(Filter<PenilaianFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(PenilaianModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(PenilaianModel.class);
    }

    public PenilaianModel findByKdNilai(int kdNilai) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(PenilaianModel.class)
                .where(PenilaianModel._KD_NILAI).isEqual(kdNilai)
                .getResult(DatabasePool.getConnection())
                .executeItem(PenilaianModel.class);
    }
}