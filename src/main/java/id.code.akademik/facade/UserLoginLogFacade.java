package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.UserLoginLogModel;
import id.code.akademik.filter.UserLoginLogFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class UserLoginLogFacade {
    public boolean insert(UserLoginLogModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(UserLoginLogModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long idUll) throws SQLException, QueryBuilderException, IOException {
        final UserLoginLogModel oldData = this.findByIdUll(idUll);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<UserLoginLogModel> getAll(Filter<UserLoginLogFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(UserLoginLogModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(UserLoginLogModel.class);
    }

    public UserLoginLogModel findByIdUll(long idUll) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(UserLoginLogModel.class)
                .where(UserLoginLogModel._ID_ULL).isEqual(idUll)
                .getResult(DatabasePool.getConnection())
                .executeItem(UserLoginLogModel.class);
    }
}