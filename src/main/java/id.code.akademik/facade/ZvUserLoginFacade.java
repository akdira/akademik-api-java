package id.code.akademik.facade;

import id.code.akademik.filter.UserLoginFilter;
import id.code.akademik.model.ZvUserLoginModel;
import id.code.akademik.validation.UpdateUserLoginValidation;
import id.code.component.DatabasePool;
import id.code.component.PasswordHasher;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.UpdateResult;
import id.code.database.filter.Filter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class ZvUserLoginFacade {
    public boolean insert(ZvUserLoginModel value) throws SQLException, QueryBuilderException, IOException, GeneralSecurityException  {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(ZvUserLoginModel new_data) throws SQLException, IOException, GeneralSecurityException {


        final UpdateResult result = QueryBuilder.update(new_data).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(int idUser) throws SQLException, QueryBuilderException, IOException {
        final ZvUserLoginModel oldData = this.findByIdUser(idUser);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<ZvUserLoginModel> getAll(Filter<UserLoginFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(ZvUserLoginModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(ZvUserLoginModel.class);
    }

    public ZvUserLoginModel findByIdUser(int idUser) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(ZvUserLoginModel.class)
            .where(ZvUserLoginModel._ID_USER).isEqual(idUser)
            .getResult(DatabasePool.getConnection())
            .executeItem(ZvUserLoginModel.class);
    }

    public ZvUserLoginModel login(String username, String password) throws SQLException, GeneralSecurityException, UnsupportedEncodingException, QueryBuilderException {
        final ZvUserLoginModel model = QueryBuilder.select(ZvUserLoginModel.class)
                .where(ZvUserLoginModel._USERNAME).isEqual(username).limit(1)
                .getResult(DatabasePool.getConnection())
                .executeItem(ZvUserLoginModel.class);

        return model != null && PasswordHasher.verify(password, model.getPassword()) ? model : null;
    }

    public boolean isUsernameAvailable(String username) throws SQLException, QueryBuilderException {
        try (ResultBuilder result = QueryBuilder.select(ZvUserLoginModel.class, ZvUserLoginModel._ID_USER)
                .where(ZvUserLoginModel._USERNAME).isEqual(username).limit(1)
                .execute(DatabasePool.getConnection())) {
            return !result.moveNext();
        }
    }

    public boolean changePassword(ZvUserLoginModel model, String newPassword) throws SQLException, GeneralSecurityException, UnsupportedEncodingException, QueryBuilderException {
        UpdateResult result = QueryBuilder.update(model, ZvUserLoginModel._PASSWORD)
                .execute(DatabasePool.getConnection());
        return result.isModified();
    }
}