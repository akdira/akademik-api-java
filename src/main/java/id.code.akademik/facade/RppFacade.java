package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.RppModel;
import id.code.akademik.filter.RppFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class RppFacade {
    public boolean insert(RppModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(RppModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long idRpp) throws SQLException, QueryBuilderException, IOException {
        final RppModel oldData = this.findByIdRpp(idRpp);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<RppModel> getAll(Filter<RppFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(RppModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(RppModel.class);
    }

    public RppModel findByIdRpp(long idRpp) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(RppModel.class)
                .where(RppModel._ID_RPP).isEqual(idRpp)
                .getResult(DatabasePool.getConnection())
                .executeItem(RppModel.class);
    }
}