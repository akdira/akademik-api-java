package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.JurusanModel;
import id.code.akademik.filter.JurusanFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class JurusanFacade {
    public boolean insert(JurusanModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(JurusanModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(String kdJur) throws SQLException, QueryBuilderException, IOException {
        final JurusanModel oldData = this.findByKdJur(kdJur);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<JurusanModel> getAll(Filter<JurusanFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(JurusanModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(JurusanModel.class);
    }

    public JurusanModel findByKdJur(String kdJur) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(JurusanModel.class)
                .where(JurusanModel._KD_JUR).isEqual(kdJur)
                .getResult(DatabasePool.getConnection())
                .executeItem(JurusanModel.class);
    }
}