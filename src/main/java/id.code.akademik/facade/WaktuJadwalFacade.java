package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.WaktuJadwalModel;
import id.code.akademik.filter.WaktuJadwalFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class WaktuJadwalFacade {
    public boolean insert(WaktuJadwalModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(WaktuJadwalModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(String kdWaktu) throws SQLException, QueryBuilderException, IOException {
        final WaktuJadwalModel oldData = this.findByKdWaktu(kdWaktu);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<WaktuJadwalModel> getAll(Filter<WaktuJadwalFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(WaktuJadwalModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(WaktuJadwalModel.class);
    }

    public WaktuJadwalModel findByKdWaktu(String kdWaktu) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(WaktuJadwalModel.class)
                .where(WaktuJadwalModel._KD_WAKTU).isEqual(kdWaktu)
                .getResult(DatabasePool.getConnection())
                .executeItem(WaktuJadwalModel.class);
    }
}