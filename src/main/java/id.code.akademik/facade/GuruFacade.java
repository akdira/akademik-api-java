package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.GuruModel;
import id.code.akademik.filter.GuruFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class GuruFacade {
    public boolean insert(GuruModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(GuruModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(String nik) throws SQLException, QueryBuilderException, IOException {
        final GuruModel oldData = this.findByNik(nik);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<GuruModel> getAll(Filter<GuruFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(GuruModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(GuruModel.class);
    }

    public GuruModel findByNik(String nik) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(GuruModel.class)
                .where(GuruModel._NIK).isEqual(nik)
                .getResult(DatabasePool.getConnection())
                .executeItem(GuruModel.class);
    }
}