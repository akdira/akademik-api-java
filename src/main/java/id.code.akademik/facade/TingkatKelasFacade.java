package id.code.akademik.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.akademik.model.TingkatKelasModel;
import id.code.akademik.filter.TingkatKelasFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class TingkatKelasFacade {
    public boolean insert(TingkatKelasModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(TingkatKelasModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(String idTingkatKls) throws SQLException, QueryBuilderException, IOException {
        final TingkatKelasModel oldData = this.findByIdTingkatKls(idTingkatKls);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<TingkatKelasModel> getAll(Filter<TingkatKelasFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(TingkatKelasModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(TingkatKelasModel.class);
    }

    public TingkatKelasModel findByIdTingkatKls(String idTingkatKls) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(TingkatKelasModel.class)
                .where(TingkatKelasModel._ID_TINGKAT_KLS).isEqual(idTingkatKls)
                .getResult(DatabasePool.getConnection())
                .executeItem(TingkatKelasModel.class);
    }
}