package id.code.akademik;

import id.code.component.utility.StreamUtility;
import id.code.server.ApiServerApplication;

import javax.servlet.annotation.WebListener;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletContextEvent;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@WebListener
public class ServletApplication extends Application implements ServletContextListener {
    private ApiServerApplication application;

    @Override
    public void contextInitialized(ServletContextEvent event) {
        // use absolute properties path for servlet, servlet relative path is unpredictable!
        this.application = createApplication("D://projects/akademik.properties").servletStart();
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        StreamUtility.close(this.application);
    }
}