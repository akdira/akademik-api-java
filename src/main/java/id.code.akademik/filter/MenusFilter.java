package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.MenusModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class MenusFilter {
	@FilterColumn(MenusModel._ID)
	private Integer id;

	@FilterColumn(MenusModel._PARENT)
	private Integer parent;

	@FilterColumn(MenusModel._URUTAN)
	private Integer urutan;

	@FilterColumn(MenusModel._NM_LINK)
	private String nmLink;

	@FilterColumn(MenusModel._URL)
	private String url;

	@FilterColumn(MenusModel._ICON)
	private String icon;

	@FilterColumn(MenusModel._HAK_AKSES)
	private String hakAkses;

	@FilterColumn(MenusModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(MenusModel._DATE_UPDATE)
	private Date dateUpdate;


	public Integer getId() { return this.id; }
	public Integer getParent() { return this.parent; }
	public Integer getUrutan() { return this.urutan; }
	public String getNmLink() { return this.nmLink; }
	public String getUrl() { return this.url; }
	public String getIcon() { return this.icon; }
	public String getHakAkses() { return this.hakAkses; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setId(Integer id) { this.id = id; }
	public void setParent(Integer parent) { this.parent = parent; }
	public void setUrutan(Integer urutan) { this.urutan = urutan; }
	public void setNmLink(String nmLink) { this.nmLink = nmLink; }
	public void setUrl(String url) { this.url = url; }
	public void setIcon(String icon) { this.icon = icon; }
	public void setHakAkses(String hakAkses) { this.hakAkses = hakAkses; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}