package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.MataPelajaranModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class MataPelajaranFilter {
	@FilterColumn(MataPelajaranModel._KD_PEL)
	private String kdPel;

	@FilterColumn(MataPelajaranModel._NM_PEL)
	private String nmPel;

	@FilterColumn(MataPelajaranModel._KD_KEL)
	private Integer kdKel;

	@FilterColumn(MataPelajaranModel._KD_SUBKEL)
	private Integer kdSubkel;

	@FilterColumn(MataPelajaranModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(MataPelajaranModel._DATE_UPDATE)
	private Date dateUpdate;


	public String getKdPel() { return this.kdPel; }
	public String getNmPel() { return this.nmPel; }
	public Integer getKdKel() { return this.kdKel; }
	public Integer getKdSubkel() { return this.kdSubkel; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdPel(String kdPel) { this.kdPel = kdPel; }
	public void setNmPel(String nmPel) { this.nmPel = nmPel; }
	public void setKdKel(Integer kdKel) { this.kdKel = kdKel; }
	public void setKdSubkel(Integer kdSubkel) { this.kdSubkel = kdSubkel; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}