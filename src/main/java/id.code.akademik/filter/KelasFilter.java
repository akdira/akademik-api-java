package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.KelasModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class KelasFilter {
	@FilterColumn(KelasModel._KD_KLS)
	private String kdKls;

	@FilterColumn(KelasModel._KD_JUR)
	private String kdJur;

	@FilterColumn(KelasModel._NM_KLS)
	private String nmKls;

	@FilterColumn(KelasModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(KelasModel._DATE_UPDATE)
	private Date dateUpdate;


	public String getKdKls() { return this.kdKls; }
	public String getKdJur() { return this.kdJur; }
	public String getNmKls() { return this.nmKls; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdKls(String kdKls) { this.kdKls = kdKls; }
	public void setKdJur(String kdJur) { this.kdJur = kdJur; }
	public void setNmKls(String nmKls) { this.nmKls = nmKls; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}