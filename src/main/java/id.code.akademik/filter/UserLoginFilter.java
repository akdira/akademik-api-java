package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.UserLoginModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class UserLoginFilter {
	@FilterColumn(UserLoginModel._ID_USER)
	private Integer idUser;

	@FilterColumn(UserLoginModel._ID_AKSES)
	private Integer idAkses;

	@FilterColumn(UserLoginModel._USERNAME)
	private String username;

	@FilterColumn(UserLoginModel._PASSWORD)
	private String password;

	@FilterColumn(UserLoginModel._EMAIL)
	private String email;

	@FilterColumn(UserLoginModel._PHOTO)
	private String photo;

	@FilterColumn(UserLoginModel._DATE_LOGIN)
	private Date dateLogin;

	@FilterColumn(UserLoginModel._DATE_LOGOUT)
	private Date dateLogout;

	@FilterColumn(UserLoginModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(UserLoginModel._DATE_UPDATE)
	private Date dateUpdate;

	@FilterColumn(UserLoginModel._ACCESS_THNPEL)
	private String accessThnpel;

	@FilterColumn(UserLoginModel._PHOTO_THUMB)
	private String photoThumb;

	@FilterColumn(UserLoginModel._RESET_PASSWORD)
	private String resetPassword;


	public Integer getIdUser() { return this.idUser; }
	public Integer getIdAkses() { return this.idAkses; }
	public String getUsername() { return this.username; }
	public String getPassword() { return this.password; }
	public String getEmail() { return this.email; }
	public String getPhoto() { return this.photo; }
	public Date getDateLogin() { return this.dateLogin; }
	public Date getDateLogout() { return this.dateLogout; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }
	public String getAccessThnpel() { return this.accessThnpel; }
	public String getPhotoThumb() { return this.photoThumb; }
	public String getResetPassword() { return this.resetPassword; }

	public void setIdUser(Integer idUser) { this.idUser = idUser; }
	public void setIdAkses(Integer idAkses) { this.idAkses = idAkses; }
	public void setUsername(String username) { this.username = username; }
	public void setPassword(String password) { this.password = password; }
	public void setEmail(String email) { this.email = email; }
	public void setPhoto(String photo) { this.photo = photo; }
	public void setDateLogin(Date dateLogin) { this.dateLogin = dateLogin; }
	public void setDateLogout(Date dateLogout) { this.dateLogout = dateLogout; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }
	public void setAccessThnpel(String accessThnpel) { this.accessThnpel = accessThnpel; }
	public void setPhotoThumb(String photoThumb) { this.photoThumb = photoThumb; }
	public void setResetPassword(String resetPassword) { this.resetPassword = resetPassword; }

}