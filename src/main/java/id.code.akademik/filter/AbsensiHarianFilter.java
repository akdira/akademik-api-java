package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.AbsensiHarianModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class AbsensiHarianFilter {
	@FilterColumn(AbsensiHarianModel._KD_ABSEN)
	private Integer kdAbsen;

	@FilterColumn(AbsensiHarianModel._KD_KLS)
	private String kdKls;

	@FilterColumn(AbsensiHarianModel._KD_THN)
	private String kdThn;

	@FilterColumn(AbsensiHarianModel._NIK)
	private String nik;

	@FilterColumn(AbsensiHarianModel._SEM)
	private Integer sem;

	@FilterColumn(AbsensiHarianModel._TANGGAL)
	private Date tanggal;


	public Integer getKdAbsen() { return this.kdAbsen; }
	public String getKdKls() { return this.kdKls; }
	public String getKdThn() { return this.kdThn; }
	public String getNik() { return this.nik; }
	public Integer getSem() { return this.sem; }
	public Date getTanggal() { return this.tanggal; }

	public void setKdAbsen(Integer kdAbsen) { this.kdAbsen = kdAbsen; }
	public void setKdKls(String kdKls) { this.kdKls = kdKls; }
	public void setKdThn(String kdThn) { this.kdThn = kdThn; }
	public void setNik(String nik) { this.nik = nik; }
	public void setSem(Integer sem) { this.sem = sem; }
	public void setTanggal(Date tanggal) { this.tanggal = tanggal; }

}