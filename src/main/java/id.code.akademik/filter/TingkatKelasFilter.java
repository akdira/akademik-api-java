package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.TingkatKelasModel;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class TingkatKelasFilter {
	@FilterColumn(TingkatKelasModel._ID_TINGKAT_KLS)
	private String idTingkatKls;

	@FilterColumn(TingkatKelasModel._NO_URUT)
	private Integer noUrut;


	public String getIdTingkatKls() { return this.idTingkatKls; }
	public Integer getNoUrut() { return this.noUrut; }

	public void setIdTingkatKls(String idTingkatKls) { this.idTingkatKls = idTingkatKls; }
	public void setNoUrut(Integer noUrut) { this.noUrut = noUrut; }

}