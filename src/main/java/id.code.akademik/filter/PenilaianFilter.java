package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.PenilaianModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class PenilaianFilter {
	@FilterColumn(PenilaianModel._KD_NILAI)
	private Integer kdNilai;

	@FilterColumn(PenilaianModel._KD_THN)
	private String kdThn;

	@FilterColumn(PenilaianModel._KD_KLS)
	private String kdKls;

	@FilterColumn(PenilaianModel._KD_PEL)
	private String kdPel;

	@FilterColumn(PenilaianModel._NIK)
	private String nik;

	@FilterColumn(PenilaianModel._SEM)
	private Integer sem;

	@FilterColumn(PenilaianModel._TYPE)
	private String type;

	@FilterColumn(PenilaianModel._KKM)
	private Double kkm;

	@FilterColumn(PenilaianModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(PenilaianModel._DATE_UPDATE)
	private Date dateUpdate;


	public Integer getKdNilai() { return this.kdNilai; }
	public String getKdThn() { return this.kdThn; }
	public String getKdKls() { return this.kdKls; }
	public String getKdPel() { return this.kdPel; }
	public String getNik() { return this.nik; }
	public Integer getSem() { return this.sem; }
	public String getType() { return this.type; }
	public Double getKkm() { return this.kkm; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdNilai(Integer kdNilai) { this.kdNilai = kdNilai; }
	public void setKdThn(String kdThn) { this.kdThn = kdThn; }
	public void setKdKls(String kdKls) { this.kdKls = kdKls; }
	public void setKdPel(String kdPel) { this.kdPel = kdPel; }
	public void setNik(String nik) { this.nik = nik; }
	public void setSem(Integer sem) { this.sem = sem; }
	public void setType(String type) { this.type = type; }
	public void setKkm(Double kkm) { this.kkm = kkm; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}