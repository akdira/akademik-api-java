package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.AbsensiHarianDetailModel;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class AbsensiHarianDetailFilter {
	@FilterColumn(AbsensiHarianDetailModel._KD_ABSEN_DETAIL)
	private Integer kdAbsenDetail;

	@FilterColumn(AbsensiHarianDetailModel._KD_ABSEN)
	private Integer kdAbsen;

	@FilterColumn(AbsensiHarianDetailModel._NIS)
	private String nis;

	@FilterColumn(AbsensiHarianDetailModel._STATUS)
	private char status;

	@FilterColumn(AbsensiHarianDetailModel._KETERANGAN)
	private String keterangan;


	public Integer getKdAbsenDetail() { return this.kdAbsenDetail; }
	public Integer getKdAbsen() { return this.kdAbsen; }
	public String getNis() { return this.nis; }
	public char getStatus() { return this.status; }
	public String getKeterangan() { return this.keterangan; }

	public void setKdAbsenDetail(Integer kdAbsenDetail) { this.kdAbsenDetail = kdAbsenDetail; }
	public void setKdAbsen(Integer kdAbsen) { this.kdAbsen = kdAbsen; }
	public void setNis(String nis) { this.nis = nis; }
	public void setStatus(char status) { this.status = status; }
	public void setKeterangan(String keterangan) { this.keterangan = keterangan; }

}