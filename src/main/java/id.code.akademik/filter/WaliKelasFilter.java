package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.WaliKelasModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class WaliKelasFilter {
	@FilterColumn(WaliKelasModel._KD_WK)
	private Long kdWk;

	@FilterColumn(WaliKelasModel._KD_THN)
	private String kdThn;

	@FilterColumn(WaliKelasModel._NIK)
	private String nik;

	@FilterColumn(WaliKelasModel._KD_KLS)
	private String kdKls;

	@FilterColumn(WaliKelasModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(WaliKelasModel._DATE_UPDATE)
	private Date dateUpdate;


	public Long getKdWk() { return this.kdWk; }
	public String getKdThn() { return this.kdThn; }
	public String getNik() { return this.nik; }
	public String getKdKls() { return this.kdKls; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdWk(Long kdWk) { this.kdWk = kdWk; }
	public void setKdThn(String kdThn) { this.kdThn = kdThn; }
	public void setNik(String nik) { this.nik = nik; }
	public void setKdKls(String kdKls) { this.kdKls = kdKls; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}