package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.PenilaianDetailModel;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class PenilaianDetailFilter {
	@FilterColumn(PenilaianDetailModel._KD_NILAI_DETAIL)
	private Integer kdNilaiDetail;

	@FilterColumn(PenilaianDetailModel._KD_NILAI)
	private Integer kdNilai;

	@FilterColumn(PenilaianDetailModel._NIS)
	private String nis;

	@FilterColumn(PenilaianDetailModel._NILAI)
	private Double nilai;

	@FilterColumn(PenilaianDetailModel._CATATAN)
	private String catatan;


	public Integer getKdNilaiDetail() { return this.kdNilaiDetail; }
	public Integer getKdNilai() { return this.kdNilai; }
	public String getNis() { return this.nis; }
	public Double getNilai() { return this.nilai; }
	public String getCatatan() { return this.catatan; }

	public void setKdNilaiDetail(Integer kdNilaiDetail) { this.kdNilaiDetail = kdNilaiDetail; }
	public void setKdNilai(Integer kdNilai) { this.kdNilai = kdNilai; }
	public void setNis(String nis) { this.nis = nis; }
	public void setNilai(Double nilai) { this.nilai = nilai; }
	public void setCatatan(String catatan) { this.catatan = catatan; }

}