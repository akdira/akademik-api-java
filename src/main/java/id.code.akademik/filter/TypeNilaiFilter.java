package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.TypeNilaiModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class TypeNilaiFilter {
	@FilterColumn(TypeNilaiModel._KD_TYPE_NILAI)
	private String kdTypeNilai;

	@FilterColumn(TypeNilaiModel._NM_TYPE_NILAI)
	private String nmTypeNilai;

	@FilterColumn(TypeNilaiModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(TypeNilaiModel._DATE_UPDATE)
	private Date dateUpdate;


	public String getKdTypeNilai() { return this.kdTypeNilai; }
	public String getNmTypeNilai() { return this.nmTypeNilai; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdTypeNilai(String kdTypeNilai) { this.kdTypeNilai = kdTypeNilai; }
	public void setNmTypeNilai(String nmTypeNilai) { this.nmTypeNilai = nmTypeNilai; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}