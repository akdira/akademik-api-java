package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.HakAksesModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class HakAksesFilter {
	@FilterColumn(HakAksesModel._ID_AKSES)
	private Integer idAkses;

	@FilterColumn(HakAksesModel._NM_AKSES)
	private String nmAkses;

	@FilterColumn(HakAksesModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(HakAksesModel._DATE_UPDATE)
	private Date dateUpdate;


	public Integer getIdAkses() { return this.idAkses; }
	public String getNmAkses() { return this.nmAkses; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setIdAkses(Integer idAkses) { this.idAkses = idAkses; }
	public void setNmAkses(String nmAkses) { this.nmAkses = nmAkses; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}