package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.JadwalMatapelModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class JadwalMatapelFilter {
	@FilterColumn(JadwalMatapelModel._KD_JADWAL)
	private Long kdJadwal;

	@FilterColumn(JadwalMatapelModel._KD_THN)
	private String kdThn;

	@FilterColumn(JadwalMatapelModel._KD_KLS)
	private String kdKls;

	@FilterColumn(JadwalMatapelModel._KD_PEL)
	private String kdPel;

	@FilterColumn(JadwalMatapelModel._NIK)
	private String nik;

	@FilterColumn(JadwalMatapelModel._KD_RUANG)
	private String kdRuang;

	@FilterColumn(JadwalMatapelModel._SEM)
	private char sem;

	@FilterColumn(JadwalMatapelModel._HARI)
	private String hari;

	@FilterColumn(JadwalMatapelModel._JAM_AWAL)
	private Integer jamAwal;

	@FilterColumn(JadwalMatapelModel._JAM_AKHIR)
	private Integer jamAkhir;

	@FilterColumn(JadwalMatapelModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(JadwalMatapelModel._DATE_UPDATE)
	private Date dateUpdate;


	public Long getKdJadwal() { return this.kdJadwal; }
	public String getKdThn() { return this.kdThn; }
	public String getKdKls() { return this.kdKls; }
	public String getKdPel() { return this.kdPel; }
	public String getNik() { return this.nik; }
	public String getKdRuang() { return this.kdRuang; }
	public char getSem() { return this.sem; }
	public String getHari() { return this.hari; }
	public Integer getJamAwal() { return this.jamAwal; }
	public Integer getJamAkhir() { return this.jamAkhir; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdJadwal(Long kdJadwal) { this.kdJadwal = kdJadwal; }
	public void setKdThn(String kdThn) { this.kdThn = kdThn; }
	public void setKdKls(String kdKls) { this.kdKls = kdKls; }
	public void setKdPel(String kdPel) { this.kdPel = kdPel; }
	public void setNik(String nik) { this.nik = nik; }
	public void setKdRuang(String kdRuang) { this.kdRuang = kdRuang; }
	public void setSem(char sem) { this.sem = sem; }
	public void setHari(String hari) { this.hari = hari; }
	public void setJamAwal(Integer jamAwal) { this.jamAwal = jamAwal; }
	public void setJamAkhir(Integer jamAkhir) { this.jamAkhir = jamAkhir; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}