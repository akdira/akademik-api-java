package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.JabatanModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class JabatanFilter {
	@FilterColumn(JabatanModel._KD_JAB)
	private Integer kdJab;

	@FilterColumn(JabatanModel._NM_JAB)
	private String nmJab;

	@FilterColumn(JabatanModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(JabatanModel._DATE_UPDATE)
	private Date dateUpdate;


	public Integer getKdJab() { return this.kdJab; }
	public String getNmJab() { return this.nmJab; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdJab(Integer kdJab) { this.kdJab = kdJab; }
	public void setNmJab(String nmJab) { this.nmJab = nmJab; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}