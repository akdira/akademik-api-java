package id.code.akademik.filter;

import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.SiswaModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class SiswaFilter {
	@FilterColumn(SiswaModel._NIS)
	private String nis;

	@FilterColumn(SiswaModel._ID_USER)
	private Integer idUser;

	@FilterColumn(SiswaModel._NISN)
	private String nisn;

	@FilterColumn(value = SiswaModel._NM_SISWA, comparator = WhereComparator.CONTAINS)
	private String nmSiswa;

	@FilterColumn(SiswaModel._ANGKATAN)
	private String angkatan;

	@FilterColumn(SiswaModel._STATUS)
	private Integer status;

	@FilterColumn(SiswaModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(SiswaModel._DATE_UPDATE)
	private Date dateUpdate;


	public String getNis() { return this.nis; }
	public Integer getIdUser() { return this.idUser; }
	public String getNisn() { return this.nisn; }
	public String getNmSiswa() { return this.nmSiswa; }
	public String getAngkatan() { return this.angkatan; }
	public Integer getStatus() { return this.status; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setNis(String nis) { this.nis = nis; }
	public void setIdUser(Integer idUser) { this.idUser = idUser; }
	public void setNisn(String nisn) { this.nisn = nisn; }
	public void setNmSiswa(String nmSiswa) { this.nmSiswa = nmSiswa; }
	public void setAngkatan(String angkatan) { this.angkatan = angkatan; }
	public void setStatus(Integer status) { this.status = status; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}