package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.TahunPelajaranModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class TahunPelajaranFilter {
	@FilterColumn(TahunPelajaranModel._KD_THN)
	private String kdThn;

	@FilterColumn(TahunPelajaranModel._NM_THN)
	private String nmThn;

	@FilterColumn(TahunPelajaranModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(TahunPelajaranModel._SEM)
	private Integer sem;

	@FilterColumn(TahunPelajaranModel._DATE_UPDATE)
	private Date dateUpdate;


	public String getKdThn() { return this.kdThn; }
	public String getNmThn() { return this.nmThn; }
	public Date getDateCreate() { return this.dateCreate; }
	public Integer getSem() { return this.sem; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdThn(String kdThn) { this.kdThn = kdThn; }
	public void setNmThn(String nmThn) { this.nmThn = nmThn; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setSem(Integer sem) { this.sem = sem; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}