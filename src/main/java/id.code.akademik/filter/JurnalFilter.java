package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.JurnalModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class JurnalFilter {
	@FilterColumn(JurnalModel._KD_JURNAL)
	private Integer kdJurnal;

	@FilterColumn(JurnalModel._KD_THN)
	private String kdThn;

	@FilterColumn(JurnalModel._KD_KLS)
	private String kdKls;

	@FilterColumn(JurnalModel._KD_PEL)
	private String kdPel;

	@FilterColumn(JurnalModel._NIK)
	private String nik;

	@FilterColumn(JurnalModel._SEM)
	private Integer sem;

	@FilterColumn(JurnalModel._TANGGAL)
	private Date tanggal;

	@FilterColumn(JurnalModel._CATATAN_BELAJAR)
	private String catatanBelajar;


	public Integer getKdJurnal() { return this.kdJurnal; }
	public String getKdThn() { return this.kdThn; }
	public String getKdKls() { return this.kdKls; }
	public String getKdPel() { return this.kdPel; }
	public String getNik() { return this.nik; }
	public Integer getSem() { return this.sem; }
	public Date getTanggal() { return this.tanggal; }
	public String getCatatanBelajar() { return this.catatanBelajar; }

	public void setKdJurnal(Integer kdJurnal) { this.kdJurnal = kdJurnal; }
	public void setKdThn(String kdThn) { this.kdThn = kdThn; }
	public void setKdKls(String kdKls) { this.kdKls = kdKls; }
	public void setKdPel(String kdPel) { this.kdPel = kdPel; }
	public void setNik(String nik) { this.nik = nik; }
	public void setSem(Integer sem) { this.sem = sem; }
	public void setTanggal(Date tanggal) { this.tanggal = tanggal; }
	public void setCatatanBelajar(String catatanBelajar) { this.catatanBelajar = catatanBelajar; }

}