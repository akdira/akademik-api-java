package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.RppModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class RppFilter {
	@FilterColumn(RppModel._ID_RPP)
	private Long idRpp;

	@FilterColumn(RppModel._KD_THN)
	private String kdThn;

	@FilterColumn(RppModel._KD_PEL)
	private String kdPel;

	@FilterColumn(RppModel._TINGKAT_KLS)
	private String tingkatKls;

	@FilterColumn(RppModel._NIK)
	private String nik;

	@FilterColumn(RppModel._FILE_RPP)
	private String fileRpp;

	@FilterColumn(RppModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(RppModel._DATE_UPDATE)
	private Date dateUpdate;


	public Long getIdRpp() { return this.idRpp; }
	public String getKdThn() { return this.kdThn; }
	public String getKdPel() { return this.kdPel; }
	public String getTingkatKls() { return this.tingkatKls; }
	public String getNik() { return this.nik; }
	public String getFileRpp() { return this.fileRpp; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setIdRpp(Long idRpp) { this.idRpp = idRpp; }
	public void setKdThn(String kdThn) { this.kdThn = kdThn; }
	public void setKdPel(String kdPel) { this.kdPel = kdPel; }
	public void setTingkatKls(String tingkatKls) { this.tingkatKls = tingkatKls; }
	public void setNik(String nik) { this.nik = nik; }
	public void setFileRpp(String fileRpp) { this.fileRpp = fileRpp; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}