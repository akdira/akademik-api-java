package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.ProfileSekolahModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class ProfileSekolahFilter {
	@FilterColumn(ProfileSekolahModel._ID_SEKOLAH)
	private Integer idSekolah;

	@FilterColumn(ProfileSekolahModel._NAMA_SEKOLAH)
	private String namaSekolah;

	@FilterColumn(ProfileSekolahModel._ALAMAT_SEKOLAH)
	private String alamatSekolah;

	@FilterColumn(ProfileSekolahModel._EMAIL_SEKOLAH)
	private String emailSekolah;

	@FilterColumn(ProfileSekolahModel._WEBSITE_SEKOLAH)
	private String websiteSekolah;

	@FilterColumn(ProfileSekolahModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(ProfileSekolahModel._DATE_UPDATE)
	private Date dateUpdate;


	public Integer getIdSekolah() { return this.idSekolah; }
	public String getNamaSekolah() { return this.namaSekolah; }
	public String getAlamatSekolah() { return this.alamatSekolah; }
	public String getEmailSekolah() { return this.emailSekolah; }
	public String getWebsiteSekolah() { return this.websiteSekolah; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setIdSekolah(Integer idSekolah) { this.idSekolah = idSekolah; }
	public void setNamaSekolah(String namaSekolah) { this.namaSekolah = namaSekolah; }
	public void setAlamatSekolah(String alamatSekolah) { this.alamatSekolah = alamatSekolah; }
	public void setEmailSekolah(String emailSekolah) { this.emailSekolah = emailSekolah; }
	public void setWebsiteSekolah(String websiteSekolah) { this.websiteSekolah = websiteSekolah; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}