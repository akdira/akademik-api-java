package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.GuruModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class GuruFilter {
	@FilterColumn(GuruModel._NIK)
	private String nik;

	@FilterColumn(GuruModel._ID_USER)
	private Integer idUser;

	@FilterColumn(GuruModel._KD_JAB)
	private Integer kdJab;

	@FilterColumn(GuruModel._NM_GURU)
	private String nmGuru;

	@FilterColumn(GuruModel._NO_URUT)
	private Integer noUrut;

	@FilterColumn(GuruModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(GuruModel._DATE_UPDATE)
	private Date dateUpdate;


	public String getNik() { return this.nik; }
	public Integer getIdUser() { return this.idUser; }
	public Integer getKdJab() { return this.kdJab; }
	public String getNmGuru() { return this.nmGuru; }
	public Integer getNoUrut() { return this.noUrut; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setNik(String nik) { this.nik = nik; }
	public void setIdUser(Integer idUser) { this.idUser = idUser; }
	public void setKdJab(Integer kdJab) { this.kdJab = kdJab; }
	public void setNmGuru(String nmGuru) { this.nmGuru = nmGuru; }
	public void setNoUrut(Integer noUrut) { this.noUrut = noUrut; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}