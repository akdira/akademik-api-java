package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.RuanganModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class RuanganFilter {
	@FilterColumn(RuanganModel._KD_RUANG)
	private String kdRuang;

	@FilterColumn(RuanganModel._NM_RUANG)
	private String nmRuang;

	@FilterColumn(RuanganModel._KAPASITAS)
	private Integer kapasitas;

	@FilterColumn(RuanganModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(RuanganModel._DATE_UPDATE)
	private Date dateUpdate;


	public String getKdRuang() { return this.kdRuang; }
	public String getNmRuang() { return this.nmRuang; }
	public Integer getKapasitas() { return this.kapasitas; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdRuang(String kdRuang) { this.kdRuang = kdRuang; }
	public void setNmRuang(String nmRuang) { this.nmRuang = nmRuang; }
	public void setKapasitas(Integer kapasitas) { this.kapasitas = kapasitas; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}