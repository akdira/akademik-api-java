package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.GuruPiketModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class GuruPiketFilter {
	@FilterColumn(GuruPiketModel._KD_PIKET)
	private Integer kdPiket;

	@FilterColumn(GuruPiketModel._KD_THN)
	private String kdThn;

	@FilterColumn(GuruPiketModel._NIK)
	private String nik;

	@FilterColumn(GuruPiketModel._HARI)
	private String hari;

	@FilterColumn(GuruPiketModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(GuruPiketModel._DATE_UPDATE)
	private Date dateUpdate;


	public Integer getKdPiket() { return this.kdPiket; }
	public String getKdThn() { return this.kdThn; }
	public String getNik() { return this.nik; }
	public String getHari() { return this.hari; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdPiket(Integer kdPiket) { this.kdPiket = kdPiket; }
	public void setKdThn(String kdThn) { this.kdThn = kdThn; }
	public void setNik(String nik) { this.nik = nik; }
	public void setHari(String hari) { this.hari = hari; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}