package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.KelompokModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class KelompokFilter {
	@FilterColumn(KelompokModel._KD_KEL)
	private Integer kdKel;

	@FilterColumn(KelompokModel._NM_KEL)
	private String nmKel;

	@FilterColumn(KelompokModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(KelompokModel._DATE_UPDATE)
	private Date dateUpdate;


	public Integer getKdKel() { return this.kdKel; }
	public String getNmKel() { return this.nmKel; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdKel(Integer kdKel) { this.kdKel = kdKel; }
	public void setNmKel(String nmKel) { this.nmKel = nmKel; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}