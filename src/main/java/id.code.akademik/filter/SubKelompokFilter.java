package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.SubKelompokModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class SubKelompokFilter {
	@FilterColumn(SubKelompokModel._KD_SUBKEL)
	private Integer kdSubkel;

	@FilterColumn(SubKelompokModel._NM_SUBKEL)
	private String nmSubkel;

	@FilterColumn(SubKelompokModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(SubKelompokModel._DATE_UPDATE)
	private Date dateUpdate;


	public Integer getKdSubkel() { return this.kdSubkel; }
	public String getNmSubkel() { return this.nmSubkel; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdSubkel(Integer kdSubkel) { this.kdSubkel = kdSubkel; }
	public void setNmSubkel(String nmSubkel) { this.nmSubkel = nmSubkel; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}