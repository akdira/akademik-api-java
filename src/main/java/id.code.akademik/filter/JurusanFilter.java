package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.JurusanModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class JurusanFilter {
	@FilterColumn(JurusanModel._KD_JUR)
	private String kdJur;

	@FilterColumn(JurusanModel._NIK)
	private String nik;

	@FilterColumn(JurusanModel._NM_JUR)
	private String nmJur;

	@FilterColumn(JurusanModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(JurusanModel._DATE_UPDATE)
	private Date dateUpdate;


	public String getKdJur() { return this.kdJur; }
	public String getNik() { return this.nik; }
	public String getNmJur() { return this.nmJur; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdJur(String kdJur) { this.kdJur = kdJur; }
	public void setNik(String nik) { this.nik = nik; }
	public void setNmJur(String nmJur) { this.nmJur = nmJur; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}