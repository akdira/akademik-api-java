package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.HariModel;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class HariFilter {
	@FilterColumn(HariModel._KD_HARI)
	private Integer kdHari;

	@FilterColumn(HariModel._NM_HARI)
	private String nmHari;


	public Integer getKdHari() { return this.kdHari; }
	public String getNmHari() { return this.nmHari; }

	public void setKdHari(Integer kdHari) { this.kdHari = kdHari; }
	public void setNmHari(String nmHari) { this.nmHari = nmHari; }

}