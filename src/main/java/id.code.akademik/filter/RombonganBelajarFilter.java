package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.RombonganBelajarModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class RombonganBelajarFilter {
	@FilterColumn(RombonganBelajarModel._KD_ROMBEL)
	private Long kdRombel;

	@FilterColumn(RombonganBelajarModel._KD_THN)
	private String kdThn;

	@FilterColumn(RombonganBelajarModel._KD_KLS)
	private String kdKls;

	@FilterColumn(RombonganBelajarModel._NIS)
	private String nis;

	@FilterColumn(RombonganBelajarModel._DATE_CREATE)
	private Date dateCreate;

	@FilterColumn(RombonganBelajarModel._DATE_UPDATE)
	private Date dateUpdate;


	public Long getKdRombel() { return this.kdRombel; }
	public String getKdThn() { return this.kdThn; }
	public String getKdKls() { return this.kdKls; }
	public String getNis() { return this.nis; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdRombel(Long kdRombel) { this.kdRombel = kdRombel; }
	public void setKdThn(String kdThn) { this.kdThn = kdThn; }
	public void setKdKls(String kdKls) { this.kdKls = kdKls; }
	public void setNis(String nis) { this.nis = nis; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}