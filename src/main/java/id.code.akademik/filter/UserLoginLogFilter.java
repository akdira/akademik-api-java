package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.UserLoginLogModel;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class UserLoginLogFilter {
	@FilterColumn(UserLoginLogModel._ID_ULL)
	private Long idUll;

	@FilterColumn(UserLoginLogModel._ID_USER)
	private Integer idUser;

	@FilterColumn(UserLoginLogModel._LOGIN_TYPE)
	private char loginType;

	@FilterColumn(UserLoginLogModel._BROWSER)
	private String browser;

	@FilterColumn(UserLoginLogModel._BROWSER_VERSION)
	private String browserVersion;

	@FilterColumn(UserLoginLogModel._PLATFORM)
	private String platform;

	@FilterColumn(UserLoginLogModel._PARENT)
	private String parent;

	@FilterColumn(UserLoginLogModel._IP)
	private String ip;

	@FilterColumn(UserLoginLogModel._DATE_CREATE)
	private Date dateCreate;


	public Long getIdUll() { return this.idUll; }
	public Integer getIdUser() { return this.idUser; }
	public char getLoginType() { return this.loginType; }
	public String getBrowser() { return this.browser; }
	public String getBrowserVersion() { return this.browserVersion; }
	public String getPlatform() { return this.platform; }
	public String getParent() { return this.parent; }
	public String getIp() { return this.ip; }
	public Date getDateCreate() { return this.dateCreate; }

	public void setIdUll(Long idUll) { this.idUll = idUll; }
	public void setIdUser(Integer idUser) { this.idUser = idUser; }
	public void setLoginType(char loginType) { this.loginType = loginType; }
	public void setBrowser(String browser) { this.browser = browser; }
	public void setBrowserVersion(String browserVersion) { this.browserVersion = browserVersion; }
	public void setPlatform(String platform) { this.platform = platform; }
	public void setParent(String parent) { this.parent = parent; }
	public void setIp(String ip) { this.ip = ip; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }

}