package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.JurnalDetailModel;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class JurnalDetailFilter {
	@FilterColumn(JurnalDetailModel._KD_JURNAL_DETAIL)
	private Integer kdJurnalDetail;

	@FilterColumn(JurnalDetailModel._KD_JURNAL)
	private Integer kdJurnal;

	@FilterColumn(JurnalDetailModel._NIS)
	private String nis;

	@FilterColumn(JurnalDetailModel._STATUS)
	private char status;

	@FilterColumn(JurnalDetailModel._KETERANGAN)
	private String keterangan;


	public Integer getKdJurnalDetail() { return this.kdJurnalDetail; }
	public Integer getKdJurnal() { return this.kdJurnal; }
	public String getNis() { return this.nis; }
	public char getStatus() { return this.status; }
	public String getKeterangan() { return this.keterangan; }

	public void setKdJurnalDetail(Integer kdJurnalDetail) { this.kdJurnalDetail = kdJurnalDetail; }
	public void setKdJurnal(Integer kdJurnal) { this.kdJurnal = kdJurnal; }
	public void setNis(String nis) { this.nis = nis; }
	public void setStatus(char status) { this.status = status; }
	public void setKeterangan(String keterangan) { this.keterangan = keterangan; }

}