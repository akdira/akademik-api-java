package id.code.akademik.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.akademik.model.WaktuJadwalModel;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class WaktuJadwalFilter {
	@FilterColumn(WaktuJadwalModel._KD_WAKTU)
	private String kdWaktu;

	@FilterColumn(WaktuJadwalModel._WAKTU_AWAL)
	private String waktuAwal;

	@FilterColumn(WaktuJadwalModel._WAKTU_AKHIR)
	private String waktuAkhir;


	public String getKdWaktu() { return this.kdWaktu; }
	public String getWaktuAwal() { return this.waktuAwal; }
	public String getWaktuAkhir() { return this.waktuAkhir; }

	public void setKdWaktu(String kdWaktu) { this.kdWaktu = kdWaktu; }
	public void setWaktuAwal(String waktuAwal) { this.waktuAwal = waktuAwal; }
	public void setWaktuAkhir(String waktuAkhir) { this.waktuAkhir = waktuAkhir; }

}