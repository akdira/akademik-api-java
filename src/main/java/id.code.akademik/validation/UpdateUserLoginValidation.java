package id.code.akademik.validation;

import id.code.akademik.model.UserLoginModel;
import id.code.database.validation.ValidateColumn;

import java.text.ParseException;

/**
 * Created by akmal on 26/07/2018.
 */
public class UpdateUserLoginValidation extends UserLoginModel {
    @ValidateColumn
    private String password;

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    public UpdateUserLoginValidation() throws ParseException {
    }
}
