package id.code.akademik.validation;

import id.code.akademik.model.UserLoginModel;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class LoginValidation {
    @ValidateColumn(UserLoginModel._USERNAME)
    @JsonProperty(UserLoginModel._USERNAME)
    private String username;

    @ValidateColumn(UserLoginModel._PASSWORD)
    @JsonProperty(UserLoginModel._PASSWORD)
    private String password;

    public String getUsername() { return this.username; }
    public String getPassword() { return this.password; }

    public void setUsername(String username) { this.username = username; }
    public void setPassword(String password) { this.password = password; }
}