package id.code.akademik.validation;

import id.code.akademik.model.UserLoginModel;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class ChangePasswordValidation {
    @ValidateColumn(UserLoginModel._USERNAME)
    @JsonProperty(UserLoginModel._USERNAME)
    private String username;

    @ValidateColumn("old_password")
    @JsonProperty("old_password")
    private String oldPassword;

    @ValidateColumn("new_password")
    @JsonProperty("new_password")
    private String newPassword;

    public String getUsername() { return this.username; }
    public String getNewPassword() { return this.newPassword; }
    public String getOldPassword() { return this.oldPassword; }

    public void setUsername(String username) { this.username = username; }
    public void setNewPassword(String password) { this.newPassword = password; }
    public void setOldPassword(String password) { this.oldPassword = password; }
}