package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = AbsensiHarianModel.TABLE_NAME)
public class AbsensiHarianModel extends BaseModel {
    public static final String TABLE_NAME = "absensi_harian";
	public static final String _KD_ABSEN = "KD_ABSEN";
	public static final String _KD_KLS = "KD_KLS";
	public static final String _KD_THN = "KD_THN";
	public static final String _NIK = "NIK";
	public static final String _SEM = "SEM";
	public static final String _TANGGAL = "TANGGAL";

	@TableColumn(name = _KD_ABSEN, primaryKey = true, autoIncrement = true)
	@JsonProperty(_KD_ABSEN)
	private int kdAbsen;

	@TableColumn(_KD_KLS)
	@JsonProperty(_KD_KLS)
	private String kdKls;

	@TableColumn(_KD_THN)
	@JsonProperty(_KD_THN)
	private String kdThn;

	@TableColumn(_NIK)
	@JsonProperty(_NIK)
	private String nik;

	@TableColumn(_SEM)
	@JsonProperty(_SEM)
	private Integer sem;

	@TableColumn(_TANGGAL)
	@JsonProperty(_TANGGAL)
	private Date tanggal;


	public int getKdAbsen() { return this.kdAbsen; }
	public String getKdKls() { return this.kdKls; }
	public String getKdThn() { return this.kdThn; }
	public String getNik() { return this.nik; }
	public Integer getSem() { return this.sem; }
	public Date getTanggal() { return this.tanggal; }

	public void setKdAbsen(int kdAbsen) { this.kdAbsen = kdAbsen; }
	public void setKdKls(String kdKls) { this.kdKls = kdKls; }
	public void setKdThn(String kdThn) { this.kdThn = kdThn; }
	public void setNik(String nik) { this.nik = nik; }
	public void setSem(Integer sem) { this.sem = sem; }
	public void setTanggal(Date tanggal) { this.tanggal = tanggal; }

}