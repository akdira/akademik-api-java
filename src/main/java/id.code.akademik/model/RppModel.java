package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = RppModel.TABLE_NAME)
public class RppModel extends BaseModel {
    public static final String TABLE_NAME = "rpp";
	public static final String _ID_RPP = "ID_RPP";
	public static final String _KD_THN = "KD_THN";
	public static final String _KD_PEL = "KD_PEL";
	public static final String _TINGKAT_KLS = "TINGKAT_KLS";
	public static final String _NIK = "NIK";
	public static final String _FILE_RPP = "FILE_RPP";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";

	@TableColumn(name = _ID_RPP, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID_RPP)
	private long idRpp;

	@TableColumn(_KD_THN)
	@JsonProperty(_KD_THN)
	private String kdThn;

	@TableColumn(_KD_PEL)
	@JsonProperty(_KD_PEL)
	private String kdPel;

	@TableColumn(_TINGKAT_KLS)
	@JsonProperty(_TINGKAT_KLS)
	private String tingkatKls;

	@TableColumn(_NIK)
	@JsonProperty(_NIK)
	private String nik;

	@TableColumn(_FILE_RPP)
	@JsonProperty(_FILE_RPP)
	private String fileRpp;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	private Date dateUpdate;


	public long getIdRpp() { return this.idRpp; }
	public String getKdThn() { return this.kdThn; }
	public String getKdPel() { return this.kdPel; }
	public String getTingkatKls() { return this.tingkatKls; }
	public String getNik() { return this.nik; }
	public String getFileRpp() { return this.fileRpp; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setIdRpp(long idRpp) { this.idRpp = idRpp; }
	public void setKdThn(String kdThn) { this.kdThn = kdThn; }
	public void setKdPel(String kdPel) { this.kdPel = kdPel; }
	public void setTingkatKls(String tingkatKls) { this.tingkatKls = tingkatKls; }
	public void setNik(String nik) { this.nik = nik; }
	public void setFileRpp(String fileRpp) { this.fileRpp = fileRpp; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}