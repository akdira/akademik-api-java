package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = TahunPelajaranModel.TABLE_NAME)
public class TahunPelajaranModel extends BaseModel {
    public static final String TABLE_NAME = "tahun_pelajaran";
	public static final String _KD_THN = "KD_THN";
	public static final String _NM_THN = "NM_THN";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _SEM = "SEM";
	public static final String _DATE_UPDATE = "DATE_UPDATE";

	@TableColumn(name = _KD_THN, primaryKey = true)
	@JsonProperty(_KD_THN)
	@ValidateColumn(_KD_THN)
	private String kdThn;

	@TableColumn(_NM_THN)
	@JsonProperty(_NM_THN)
	private String nmThn;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_SEM)
	@JsonProperty(_SEM)
	private Integer sem;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	private Date dateUpdate;


	public String getKdThn() { return this.kdThn; }
	public String getNmThn() { return this.nmThn; }
	public Date getDateCreate() { return this.dateCreate; }
	public Integer getSem() { return this.sem; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdThn(String kdThn) { this.kdThn = kdThn; }
	public void setNmThn(String nmThn) { this.nmThn = nmThn; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setSem(Integer sem) { this.sem = sem; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}