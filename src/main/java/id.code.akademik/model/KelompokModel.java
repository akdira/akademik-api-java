package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = KelompokModel.TABLE_NAME)
public class KelompokModel extends BaseModel {
    public static final String TABLE_NAME = "kelompok";
	public static final String _KD_KEL = "KD_KEL";
	public static final String _NM_KEL = "NM_KEL";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";

	@TableColumn(name = _KD_KEL, primaryKey = true, autoIncrement = true)
	@JsonProperty(_KD_KEL)
	private int kdKel;

	@TableColumn(_NM_KEL)
	@JsonProperty(_NM_KEL)
	private String nmKel;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	private Date dateUpdate;


	public int getKdKel() { return this.kdKel; }
	public String getNmKel() { return this.nmKel; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdKel(int kdKel) { this.kdKel = kdKel; }
	public void setNmKel(String nmKel) { this.nmKel = nmKel; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}