package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = RombonganBelajarModel.TABLE_NAME)
public class RombonganBelajarModel extends BaseModel {
    public static final String TABLE_NAME = "rombongan_belajar";
	public static final String _KD_ROMBEL = "KD_ROMBEL";
	public static final String _KD_THN = "KD_THN";
	public static final String _KD_KLS = "KD_KLS";
	public static final String _NIS = "NIS";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";

	@TableColumn(name = _KD_ROMBEL, primaryKey = true, autoIncrement = true)
	@JsonProperty(_KD_ROMBEL)
	private long kdRombel;

	@TableColumn(_KD_THN)
	@JsonProperty(_KD_THN)
	@ValidateColumn(_KD_THN)
	private String kdThn;

	@TableColumn(_KD_KLS)
	@JsonProperty(_KD_KLS)
	@ValidateColumn(_KD_KLS)
	private String kdKls;

	@TableColumn(_NIS)
	@JsonProperty(_NIS)
	@ValidateColumn(_NIS)
	private String nis;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	private Date dateUpdate;


	public long getKdRombel() { return this.kdRombel; }
	public String getKdThn() { return this.kdThn; }
	public String getKdKls() { return this.kdKls; }
	public String getNis() { return this.nis; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdRombel(long kdRombel) { this.kdRombel = kdRombel; }
	public void setKdThn(String kdThn) { this.kdThn = kdThn; }
	public void setKdKls(String kdKls) { this.kdKls = kdKls; }
	public void setNis(String nis) { this.nis = nis; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}