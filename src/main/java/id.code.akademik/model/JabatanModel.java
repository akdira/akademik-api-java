package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = JabatanModel.TABLE_NAME)
public class JabatanModel extends BaseModel {
    public static final String TABLE_NAME = "jabatan";
	public static final String _KD_JAB = "KD_JAB";
	public static final String _NM_JAB = "NM_JAB";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";

	@TableColumn(name = _KD_JAB, primaryKey = true, autoIncrement = true)
	@JsonProperty(_KD_JAB)
	private int kdJab;

	@TableColumn(_NM_JAB)
	@JsonProperty(_NM_JAB)
	private String nmJab;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	private Date dateUpdate;


	public int getKdJab() { return this.kdJab; }
	public String getNmJab() { return this.nmJab; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdJab(int kdJab) { this.kdJab = kdJab; }
	public void setNmJab(String nmJab) { this.nmJab = nmJab; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}