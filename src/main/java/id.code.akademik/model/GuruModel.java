package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = GuruModel.TABLE_NAME)
public class GuruModel extends BaseModel {
    public static final String TABLE_NAME = "guru";
	public static final String _NIK = "NIK";
	public static final String _ID_USER = "ID_USER";
	public static final String _KD_JAB = "KD_JAB";
	public static final String _NM_GURU = "NM_GURU";
	public static final String _NO_URUT = "NO_URUT";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";

	@TableColumn(name = _NIK, primaryKey = true)
	@JsonProperty(_NIK)
	@ValidateColumn(_NIK)
	private String nik;

	@TableColumn(_ID_USER)
	@JsonProperty(_ID_USER)
	private Integer idUser;

	@TableColumn(_KD_JAB)
	@JsonProperty(_KD_JAB)
	private Integer kdJab;

	@TableColumn(_NM_GURU)
	@JsonProperty(_NM_GURU)
	private String nmGuru;

	@TableColumn(_NO_URUT)
	@JsonProperty(_NO_URUT)
	@ValidateColumn(_NO_URUT)
	private int noUrut;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	private Date dateUpdate;


	public String getNik() { return this.nik; }
	public Integer getIdUser() { return this.idUser; }
	public Integer getKdJab() { return this.kdJab; }
	public String getNmGuru() { return this.nmGuru; }
	public int getNoUrut() { return this.noUrut; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setNik(String nik) { this.nik = nik; }
	public void setIdUser(Integer idUser) { this.idUser = idUser; }
	public void setKdJab(Integer kdJab) { this.kdJab = kdJab; }
	public void setNmGuru(String nmGuru) { this.nmGuru = nmGuru; }
	public void setNoUrut(int noUrut) { this.noUrut = noUrut; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}