package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = PenilaianDetailModel.TABLE_NAME)
public class PenilaianDetailModel extends BaseModel {
    public static final String TABLE_NAME = "penilaian_detail";
	public static final String _KD_NILAI_DETAIL = "KD_NILAI_DETAIL";
	public static final String _KD_NILAI = "KD_NILAI";
	public static final String _NIS = "NIS";
	public static final String _NILAI = "NILAI";
	public static final String _CATATAN = "CATATAN";

	@TableColumn(name = _KD_NILAI_DETAIL, primaryKey = true, autoIncrement = true)
	@JsonProperty(_KD_NILAI_DETAIL)
	private int kdNilaiDetail;

	@TableColumn(_KD_NILAI)
	@JsonProperty(_KD_NILAI)
	private Integer kdNilai;

	@TableColumn(_NIS)
	@JsonProperty(_NIS)
	private String nis;

	@TableColumn(_NILAI)
	@JsonProperty(_NILAI)
	private Double nilai;

	@TableColumn(_CATATAN)
	@JsonProperty(_CATATAN)
	private String catatan;


	public int getKdNilaiDetail() { return this.kdNilaiDetail; }
	public Integer getKdNilai() { return this.kdNilai; }
	public String getNis() { return this.nis; }
	public Double getNilai() { return this.nilai; }
	public String getCatatan() { return this.catatan; }

	public void setKdNilaiDetail(int kdNilaiDetail) { this.kdNilaiDetail = kdNilaiDetail; }
	public void setKdNilai(Integer kdNilai) { this.kdNilai = kdNilai; }
	public void setNis(String nis) { this.nis = nis; }
	public void setNilai(Double nilai) { this.nilai = nilai; }
	public void setCatatan(String catatan) { this.catatan = catatan; }

}