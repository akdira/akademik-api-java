package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = HariModel.TABLE_NAME)
public class HariModel extends BaseModel {
    public static final String TABLE_NAME = "hari";
	public static final String _KD_HARI = "KD_HARI";
	public static final String _NM_HARI = "NM_HARI";

	@TableColumn(name = _KD_HARI, primaryKey = true, autoIncrement = true)
	@JsonProperty(_KD_HARI)
	private int kdHari;

	@TableColumn(_NM_HARI)
	@JsonProperty(_NM_HARI)
	private String nmHari;


	public int getKdHari() { return this.kdHari; }
	public String getNmHari() { return this.nmHari; }

	public void setKdHari(int kdHari) { this.kdHari = kdHari; }
	public void setNmHari(String nmHari) { this.nmHari = nmHari; }

}