package id.code.akademik.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = ListWaliKelasModel.TABLE_NAME)
public class ListWaliKelasModel extends BaseModel {
    public static final String TABLE_NAME = "wali_kelas";
	public static final String _KD_WK = "KD_WK";
	public static final String _KD_THN = "KD_THN";
	public static final String _NM_THN = "NM_THN";
	public static final String _NIK = "NIK";
	public static final String _NM_GURU = "NM_GURU";
	public static final String _KD_KLS = "KD_KLS";
	public static final String _NM_KLS = "NM_KLS";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";

	@TableColumn(name = _KD_WK, primaryKey = true, autoIncrement = true)
	@JsonProperty(_KD_WK)
	private long kdWk;

	@TableColumn(_KD_THN)
	@JsonProperty(_KD_THN)
	@ValidateColumn(_KD_THN)
	private String kdThn;

	@TableColumn(_NM_THN)
	@JsonProperty(_NM_THN)
	@ValidateColumn(_NM_THN)
	private String nmThn;

	@TableColumn(_NIK)
	@JsonProperty(_NIK)
	@ValidateColumn(_NIK)
	private String nik;

	@TableColumn(_KD_KLS)
	@JsonProperty(_KD_KLS)
	@ValidateColumn(_KD_KLS)
	private String kdKls;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	private Date dateUpdate;


	public long getKdWk() { return this.kdWk; }
	public String getKdThn() { return this.kdThn; }
	public String getNik() { return this.nik; }
	public String getKdKls() { return this.kdKls; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdWk(long kdWk) { this.kdWk = kdWk; }
	public void setKdThn(String kdThn) { this.kdThn = kdThn; }
	public void setNik(String nik) { this.nik = nik; }
	public void setKdKls(String kdKls) { this.kdKls = kdKls; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}