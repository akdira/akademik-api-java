package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = MataPelajaranModel.TABLE_NAME)
public class MataPelajaranModel extends BaseModel {
    public static final String TABLE_NAME = "mata_pelajaran";
	public static final String _KD_PEL = "KD_PEL";
	public static final String _NM_PEL = "NM_PEL";
	public static final String _KD_KEL = "KD_KEL";
	public static final String _KD_SUBKEL = "KD_SUBKEL";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";

	@TableColumn(name = _KD_PEL, primaryKey = true)
	@JsonProperty(_KD_PEL)
	@ValidateColumn(_KD_PEL)
	private String kdPel;

	@TableColumn(_NM_PEL)
	@JsonProperty(_NM_PEL)
	private String nmPel;

	@TableColumn(_KD_KEL)
	@JsonProperty(_KD_KEL)
	private Integer kdKel;

	@TableColumn(_KD_SUBKEL)
	@JsonProperty(_KD_SUBKEL)
	private Integer kdSubkel;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	private Date dateUpdate;


	public String getKdPel() { return this.kdPel; }
	public String getNmPel() { return this.nmPel; }
	public Integer getKdKel() { return this.kdKel; }
	public Integer getKdSubkel() { return this.kdSubkel; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdPel(String kdPel) { this.kdPel = kdPel; }
	public void setNmPel(String nmPel) { this.nmPel = nmPel; }
	public void setKdKel(Integer kdKel) { this.kdKel = kdKel; }
	public void setKdSubkel(Integer kdSubkel) { this.kdSubkel = kdSubkel; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}