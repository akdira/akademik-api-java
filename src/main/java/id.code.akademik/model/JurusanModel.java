package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = JurusanModel.TABLE_NAME)
public class JurusanModel extends BaseModel {
    public static final String TABLE_NAME = "jurusan";
	public static final String _KD_JUR = "KD_JUR";
	public static final String _NIK = "NIK";
	public static final String _NM_JUR = "NM_JUR";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";

	@TableColumn(name = _KD_JUR, primaryKey = true)
	@JsonProperty(_KD_JUR)
	@ValidateColumn(_KD_JUR)
	private String kdJur;

	@TableColumn(_NIK)
	@JsonProperty(_NIK)
	private String nik;

	@TableColumn(_NM_JUR)
	@JsonProperty(_NM_JUR)
	@ValidateColumn(_NM_JUR)
	private String nmJur;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	@ValidateColumn(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	@ValidateColumn(_DATE_UPDATE)
	private Date dateUpdate;


	public String getKdJur() { return this.kdJur; }
	public String getNik() { return this.nik; }
	public String getNmJur() { return this.nmJur; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdJur(String kdJur) { this.kdJur = kdJur; }
	public void setNik(String nik) { this.nik = nik; }
	public void setNmJur(String nmJur) { this.nmJur = nmJur; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}