package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = RuanganModel.TABLE_NAME)
public class RuanganModel extends BaseModel {
    public static final String TABLE_NAME = "ruangan";
	public static final String _KD_RUANG = "KD_RUANG";
	public static final String _NM_RUANG = "NM_RUANG";
	public static final String _KAPASITAS = "KAPASITAS";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";

	@TableColumn(name = _KD_RUANG, primaryKey = true)
	@JsonProperty(_KD_RUANG)
	@ValidateColumn(_KD_RUANG)
	private String kdRuang;

	@TableColumn(_NM_RUANG)
	@JsonProperty(_NM_RUANG)
	private String nmRuang;

	@TableColumn(_KAPASITAS)
	@JsonProperty(_KAPASITAS)
	private Integer kapasitas;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	private Date dateUpdate;


	public String getKdRuang() { return this.kdRuang; }
	public String getNmRuang() { return this.nmRuang; }
	public Integer getKapasitas() { return this.kapasitas; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdRuang(String kdRuang) { this.kdRuang = kdRuang; }
	public void setNmRuang(String nmRuang) { this.nmRuang = nmRuang; }
	public void setKapasitas(Integer kapasitas) { this.kapasitas = kapasitas; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}