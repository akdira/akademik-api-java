package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = SiswaModel.TABLE_NAME)
public class SiswaModel extends BaseModel {
    public static final String TABLE_NAME = "siswa";
	public static final String _NIS = "NIS";
	public static final String _ID_USER = "ID_USER";
	public static final String _NISN = "NISN";
	public static final String _NM_SISWA = "NM_SISWA";
	public static final String _ANGKATAN = "ANGKATAN";
	public static final String _STATUS = "STATUS";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";

	@TableColumn(name = _NIS, primaryKey = true)
	@JsonProperty(_NIS)
	@ValidateColumn(_NIS)
	private String nis;

	@TableColumn(_ID_USER)
	@JsonProperty(_ID_USER)
	private Integer idUser;

	@TableColumn(_NISN)
	@JsonProperty(_NISN)
	private String nisn;

	@TableColumn(_NM_SISWA)
	@JsonProperty(_NM_SISWA)
	private String nmSiswa;

	@TableColumn(_ANGKATAN)
	@JsonProperty(_ANGKATAN)
	private String angkatan;

	@TableColumn(_STATUS)
	@JsonProperty(_STATUS)
	private Integer status;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	private Date dateUpdate;


	public String getNis() { return this.nis; }
	public Integer getIdUser() { return this.idUser; }
	public String getNisn() { return this.nisn; }
	public String getNmSiswa() { return this.nmSiswa; }
	public String getAngkatan() { return this.angkatan; }
	public Integer getStatus() { return this.status; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setNis(String nis) { this.nis = nis; }
	public void setIdUser(Integer idUser) { this.idUser = idUser; }
	public void setNisn(String nisn) { this.nisn = nisn; }
	public void setNmSiswa(String nmSiswa) { this.nmSiswa = nmSiswa; }
	public void setAngkatan(String angkatan) { this.angkatan = angkatan; }
	public void setStatus(Integer status) { this.status = status; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}