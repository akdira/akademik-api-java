package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = WaktuJadwalModel.TABLE_NAME)
public class WaktuJadwalModel extends BaseModel {
    public static final String TABLE_NAME = "waktu_jadwal";
	public static final String _KD_WAKTU = "kd_waktu";
	public static final String _WAKTU_AWAL = "waktu_awal";
	public static final String _WAKTU_AKHIR = "waktu_akhir";

	@TableColumn(name = _KD_WAKTU, primaryKey = true)
	@JsonProperty(_KD_WAKTU)
	@ValidateColumn(_KD_WAKTU)
	private String kdWaktu;

	@TableColumn(_WAKTU_AWAL)
	@JsonProperty(_WAKTU_AWAL)
	@ValidateColumn(_WAKTU_AWAL)
	private String waktuAwal;

	@TableColumn(_WAKTU_AKHIR)
	@JsonProperty(_WAKTU_AKHIR)
	@ValidateColumn(_WAKTU_AKHIR)
	private String waktuAkhir;


	public String getKdWaktu() { return this.kdWaktu; }
	public String getWaktuAwal() { return this.waktuAwal; }
	public String getWaktuAkhir() { return this.waktuAkhir; }

	public void setKdWaktu(String kdWaktu) { this.kdWaktu = kdWaktu; }
	public void setWaktuAwal(String waktuAwal) { this.waktuAwal = waktuAwal; }
	public void setWaktuAkhir(String waktuAkhir) { this.waktuAkhir = waktuAkhir; }

}