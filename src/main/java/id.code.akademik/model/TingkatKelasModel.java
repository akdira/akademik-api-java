package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = TingkatKelasModel.TABLE_NAME)
public class TingkatKelasModel extends BaseModel {
    public static final String TABLE_NAME = "tingkat_kelas";
	public static final String _ID_TINGKAT_KLS = "ID_TINGKAT_KLS";
	public static final String _NO_URUT = "NO_URUT";

	@TableColumn(name = _ID_TINGKAT_KLS, primaryKey = true)
	@JsonProperty(_ID_TINGKAT_KLS)
	@ValidateColumn(_ID_TINGKAT_KLS)
	private String idTingkatKls;

	@TableColumn(_NO_URUT)
	@JsonProperty(_NO_URUT)
	private Integer noUrut;


	public String getIdTingkatKls() { return this.idTingkatKls; }
	public Integer getNoUrut() { return this.noUrut; }

	public void setIdTingkatKls(String idTingkatKls) { this.idTingkatKls = idTingkatKls; }
	public void setNoUrut(Integer noUrut) { this.noUrut = noUrut; }

}