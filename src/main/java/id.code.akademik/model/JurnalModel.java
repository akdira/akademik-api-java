package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = JurnalModel.TABLE_NAME)
public class JurnalModel extends BaseModel {
    public static final String TABLE_NAME = "jurnal";
	public static final String _KD_JURNAL = "KD_JURNAL";
	public static final String _KD_THN = "KD_THN";
	public static final String _KD_KLS = "KD_KLS";
	public static final String _KD_PEL = "KD_PEL";
	public static final String _NIK = "NIK";
	public static final String _SEM = "SEM";
	public static final String _TANGGAL = "TANGGAL";
	public static final String _CATATAN_BELAJAR = "CATATAN_BELAJAR";

	@TableColumn(name = _KD_JURNAL, primaryKey = true, autoIncrement = true)
	@JsonProperty(_KD_JURNAL)
	private int kdJurnal;

	@TableColumn(_KD_THN)
	@JsonProperty(_KD_THN)
	private String kdThn;

	@TableColumn(_KD_KLS)
	@JsonProperty(_KD_KLS)
	private String kdKls;

	@TableColumn(_KD_PEL)
	@JsonProperty(_KD_PEL)
	private String kdPel;

	@TableColumn(_NIK)
	@JsonProperty(_NIK)
	private String nik;

	@TableColumn(_SEM)
	@JsonProperty(_SEM)
	private Integer sem;

	@TableColumn(_TANGGAL)
	@JsonProperty(_TANGGAL)
	private Date tanggal;

	@TableColumn(_CATATAN_BELAJAR)
	@JsonProperty(_CATATAN_BELAJAR)
	private String catatanBelajar;


	public int getKdJurnal() { return this.kdJurnal; }
	public String getKdThn() { return this.kdThn; }
	public String getKdKls() { return this.kdKls; }
	public String getKdPel() { return this.kdPel; }
	public String getNik() { return this.nik; }
	public Integer getSem() { return this.sem; }
	public Date getTanggal() { return this.tanggal; }
	public String getCatatanBelajar() { return this.catatanBelajar; }

	public void setKdJurnal(int kdJurnal) { this.kdJurnal = kdJurnal; }
	public void setKdThn(String kdThn) { this.kdThn = kdThn; }
	public void setKdKls(String kdKls) { this.kdKls = kdKls; }
	public void setKdPel(String kdPel) { this.kdPel = kdPel; }
	public void setNik(String nik) { this.nik = nik; }
	public void setSem(Integer sem) { this.sem = sem; }
	public void setTanggal(Date tanggal) { this.tanggal = tanggal; }
	public void setCatatanBelajar(String catatanBelajar) { this.catatanBelajar = catatanBelajar; }

}