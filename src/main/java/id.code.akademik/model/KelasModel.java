package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = KelasModel.TABLE_NAME)
public class KelasModel extends BaseModel {
    public static final String TABLE_NAME = "kelas";
	public static final String _KD_KLS = "KD_KLS";
	public static final String _KD_JUR = "KD_JUR";
	public static final String _NM_KLS = "NM_KLS";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";

	@TableColumn(name = _KD_KLS, primaryKey = true)
	@JsonProperty(_KD_KLS)
	@ValidateColumn(_KD_KLS)
	private String kdKls;

	@TableColumn(_KD_JUR)
	@JsonProperty(_KD_JUR)
	private String kdJur;

	@TableColumn(_NM_KLS)
	@JsonProperty(_NM_KLS)
	private String nmKls;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	private Date dateUpdate;


	public String getKdKls() { return this.kdKls; }
	public String getKdJur() { return this.kdJur; }
	public String getNmKls() { return this.nmKls; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdKls(String kdKls) { this.kdKls = kdKls; }
	public void setKdJur(String kdJur) { this.kdJur = kdJur; }
	public void setNmKls(String nmKls) { this.nmKls = nmKls; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}