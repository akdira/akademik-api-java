package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = TypeNilaiModel.TABLE_NAME)
public class TypeNilaiModel extends BaseModel {
    public static final String TABLE_NAME = "type_nilai";
	public static final String _KD_TYPE_NILAI = "KD_TYPE_NILAI";
	public static final String _NM_TYPE_NILAI = "NM_TYPE_NILAI";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";

	@TableColumn(_KD_TYPE_NILAI)
	@JsonProperty(_KD_TYPE_NILAI)
	private String kdTypeNilai;

	@TableColumn(_NM_TYPE_NILAI)
	@JsonProperty(_NM_TYPE_NILAI)
	private String nmTypeNilai;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	private Date dateUpdate;


	public String getKdTypeNilai() { return this.kdTypeNilai; }
	public String getNmTypeNilai() { return this.nmTypeNilai; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdTypeNilai(String kdTypeNilai) { this.kdTypeNilai = kdTypeNilai; }
	public void setNmTypeNilai(String nmTypeNilai) { this.nmTypeNilai = nmTypeNilai; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}