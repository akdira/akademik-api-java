package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = JurnalDetailModel.TABLE_NAME)
public class JurnalDetailModel extends BaseModel {
    public static final String TABLE_NAME = "jurnal_detail";
	public static final String _KD_JURNAL_DETAIL = "KD_JURNAL_DETAIL";
	public static final String _KD_JURNAL = "KD_JURNAL";
	public static final String _NIS = "NIS";
	public static final String _STATUS = "STATUS";
	public static final String _KETERANGAN = "KETERANGAN";

	@TableColumn(name = _KD_JURNAL_DETAIL, primaryKey = true, autoIncrement = true)
	@JsonProperty(_KD_JURNAL_DETAIL)
	private int kdJurnalDetail;

	@TableColumn(_KD_JURNAL)
	@JsonProperty(_KD_JURNAL)
	private Integer kdJurnal;

	@TableColumn(_NIS)
	@JsonProperty(_NIS)
	private String nis;

	@TableColumn(_STATUS)
	@JsonProperty(_STATUS)
	private char status;

	@TableColumn(_KETERANGAN)
	@JsonProperty(_KETERANGAN)
	private String keterangan;


	public int getKdJurnalDetail() { return this.kdJurnalDetail; }
	public Integer getKdJurnal() { return this.kdJurnal; }
	public String getNis() { return this.nis; }
	public char getStatus() { return this.status; }
	public String getKeterangan() { return this.keterangan; }

	public void setKdJurnalDetail(int kdJurnalDetail) { this.kdJurnalDetail = kdJurnalDetail; }
	public void setKdJurnal(Integer kdJurnal) { this.kdJurnal = kdJurnal; }
	public void setNis(String nis) { this.nis = nis; }
	public void setStatus(char status) { this.status = status; }
	public void setKeterangan(String keterangan) { this.keterangan = keterangan; }

}