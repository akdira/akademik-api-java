package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = MenusModel.TABLE_NAME)
public class MenusModel extends BaseModel {
    public static final String TABLE_NAME = "menus";
	public static final String _ID = "ID";
	public static final String _PARENT = "PARENT";
	public static final String _URUTAN = "URUTAN";
	public static final String _NM_LINK = "NM_LINK";
	public static final String _URL = "URL";
	public static final String _ICON = "ICON";
	public static final String _HAK_AKSES = "HAK_AKSES";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";

	@TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID)
	private int id;

	@TableColumn(_PARENT)
	@JsonProperty(_PARENT)
	private Integer parent;

	@TableColumn(_URUTAN)
	@JsonProperty(_URUTAN)
	private Integer urutan;

	@TableColumn(_NM_LINK)
	@JsonProperty(_NM_LINK)
	private String nmLink;

	@TableColumn(_URL)
	@JsonProperty(_URL)
	private String url;

	@TableColumn(_ICON)
	@JsonProperty(_ICON)
	private String icon;

	@TableColumn(_HAK_AKSES)
	@JsonProperty(_HAK_AKSES)
	@ValidateColumn(_HAK_AKSES)
	private String hakAkses;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	private Date dateUpdate;


	public int getId() { return this.id; }
	public Integer getParent() { return this.parent; }
	public Integer getUrutan() { return this.urutan; }
	public String getNmLink() { return this.nmLink; }
	public String getUrl() { return this.url; }
	public String getIcon() { return this.icon; }
	public String getHakAkses() { return this.hakAkses; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setId(int id) { this.id = id; }
	public void setParent(Integer parent) { this.parent = parent; }
	public void setUrutan(Integer urutan) { this.urutan = urutan; }
	public void setNmLink(String nmLink) { this.nmLink = nmLink; }
	public void setUrl(String url) { this.url = url; }
	public void setIcon(String icon) { this.icon = icon; }
	public void setHakAkses(String hakAkses) { this.hakAkses = hakAkses; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}