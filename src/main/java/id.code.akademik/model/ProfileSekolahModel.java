package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = ProfileSekolahModel.TABLE_NAME)
public class ProfileSekolahModel extends BaseModel {
    public static final String TABLE_NAME = "profile_sekolah";
	public static final String _ID_SEKOLAH = "ID_SEKOLAH";
	public static final String _NAMA_SEKOLAH = "NAMA_SEKOLAH";
	public static final String _ALAMAT_SEKOLAH = "ALAMAT_SEKOLAH";
	public static final String _EMAIL_SEKOLAH = "EMAIL_SEKOLAH";
	public static final String _WEBSITE_SEKOLAH = "WEBSITE_SEKOLAH";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";

	@TableColumn(name = _ID_SEKOLAH, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID_SEKOLAH)
	private int idSekolah;

	@TableColumn(_NAMA_SEKOLAH)
	@JsonProperty(_NAMA_SEKOLAH)
	private String namaSekolah;

	@TableColumn(_ALAMAT_SEKOLAH)
	@JsonProperty(_ALAMAT_SEKOLAH)
	private String alamatSekolah;

	@TableColumn(_EMAIL_SEKOLAH)
	@JsonProperty(_EMAIL_SEKOLAH)
	private String emailSekolah;

	@TableColumn(_WEBSITE_SEKOLAH)
	@JsonProperty(_WEBSITE_SEKOLAH)
	private String websiteSekolah;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	private Date dateUpdate;


	public int getIdSekolah() { return this.idSekolah; }
	public String getNamaSekolah() { return this.namaSekolah; }
	public String getAlamatSekolah() { return this.alamatSekolah; }
	public String getEmailSekolah() { return this.emailSekolah; }
	public String getWebsiteSekolah() { return this.websiteSekolah; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setIdSekolah(int idSekolah) { this.idSekolah = idSekolah; }
	public void setNamaSekolah(String namaSekolah) { this.namaSekolah = namaSekolah; }
	public void setAlamatSekolah(String alamatSekolah) { this.alamatSekolah = alamatSekolah; }
	public void setEmailSekolah(String emailSekolah) { this.emailSekolah = emailSekolah; }
	public void setWebsiteSekolah(String websiteSekolah) { this.websiteSekolah = websiteSekolah; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}