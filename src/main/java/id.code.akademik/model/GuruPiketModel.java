package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = GuruPiketModel.TABLE_NAME)
public class GuruPiketModel extends BaseModel {
    public static final String TABLE_NAME = "guru_piket";
	public static final String _KD_PIKET = "KD_PIKET";
	public static final String _KD_THN = "KD_THN";
	public static final String _NIK = "NIK";
	public static final String _HARI = "HARI";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";

	@TableColumn(name = _KD_PIKET, primaryKey = true, autoIncrement = true)
	@JsonProperty(_KD_PIKET)
	private int kdPiket;

	@TableColumn(_KD_THN)
	@JsonProperty(_KD_THN)
	private String kdThn;

	@TableColumn(_NIK)
	@JsonProperty(_NIK)
	private String nik;

	@TableColumn(_HARI)
	@JsonProperty(_HARI)
	private String hari;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	private Date dateUpdate;


	public int getKdPiket() { return this.kdPiket; }
	public String getKdThn() { return this.kdThn; }
	public String getNik() { return this.nik; }
	public String getHari() { return this.hari; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdPiket(int kdPiket) { this.kdPiket = kdPiket; }
	public void setKdThn(String kdThn) { this.kdThn = kdThn; }
	public void setNik(String nik) { this.nik = nik; }
	public void setHari(String hari) { this.hari = hari; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}