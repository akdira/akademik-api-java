package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = HakAksesModel.TABLE_NAME)
public class HakAksesModel extends BaseModel {
    public static final String TABLE_NAME = "hak_akses";
	public static final String _ID_AKSES = "ID_AKSES";
	public static final String _NM_AKSES = "NM_AKSES";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";

	@TableColumn(name = _ID_AKSES, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID_AKSES)
	private int idAkses;

	@TableColumn(_NM_AKSES)
	@JsonProperty(_NM_AKSES)
	private String nmAkses;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	private Date dateUpdate;


	public int getIdAkses() { return this.idAkses; }
	public String getNmAkses() { return this.nmAkses; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setIdAkses(int idAkses) { this.idAkses = idAkses; }
	public void setNmAkses(String nmAkses) { this.nmAkses = nmAkses; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}