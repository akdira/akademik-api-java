package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = UserLoginLogModel.TABLE_NAME)
public class UserLoginLogModel extends BaseModel {
    public static final String TABLE_NAME = "user_login_log";
	public static final String _ID_ULL = "ID_ULL";
	public static final String _ID_USER = "ID_USER";
	public static final String _LOGIN_TYPE = "LOGIN_TYPE";
	public static final String _BROWSER = "BROWSER";
	public static final String _BROWSER_VERSION = "BROWSER_VERSION";
	public static final String _PLATFORM = "PLATFORM";
	public static final String _PARENT = "PARENT";
	public static final String _IP = "IP";
	public static final String _DATE_CREATE = "DATE_CREATE";

	@TableColumn(name = _ID_ULL, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID_ULL)
	private long idUll;

	@TableColumn(_ID_USER)
	@JsonProperty(_ID_USER)
	@ValidateColumn(_ID_USER)
	private int idUser;

	@TableColumn(_LOGIN_TYPE)
	@JsonProperty(_LOGIN_TYPE)
	@ValidateColumn(_LOGIN_TYPE)
	private char loginType;

	@TableColumn(_BROWSER)
	@JsonProperty(_BROWSER)
	private String browser;

	@TableColumn(_BROWSER_VERSION)
	@JsonProperty(_BROWSER_VERSION)
	private String browserVersion;

	@TableColumn(_PLATFORM)
	@JsonProperty(_PLATFORM)
	private String platform;

	@TableColumn(_PARENT)
	@JsonProperty(_PARENT)
	private String parent;

	@TableColumn(_IP)
	@JsonProperty(_IP)
	private String ip;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	private Date dateCreate;


	public long getIdUll() { return this.idUll; }
	public int getIdUser() { return this.idUser; }
	public char getLoginType() { return this.loginType; }
	public String getBrowser() { return this.browser; }
	public String getBrowserVersion() { return this.browserVersion; }
	public String getPlatform() { return this.platform; }
	public String getParent() { return this.parent; }
	public String getIp() { return this.ip; }
	public Date getDateCreate() { return this.dateCreate; }

	public void setIdUll(long idUll) { this.idUll = idUll; }
	public void setIdUser(int idUser) { this.idUser = idUser; }
	public void setLoginType(char loginType) { this.loginType = loginType; }
	public void setBrowser(String browser) { this.browser = browser; }
	public void setBrowserVersion(String browserVersion) { this.browserVersion = browserVersion; }
	public void setPlatform(String platform) { this.platform = platform; }
	public void setParent(String parent) { this.parent = parent; }
	public void setIp(String ip) { this.ip = ip; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }

}