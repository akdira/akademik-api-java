package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = SubKelompokModel.TABLE_NAME)
public class SubKelompokModel extends BaseModel {
    public static final String TABLE_NAME = "sub_kelompok";
	public static final String _KD_SUBKEL = "KD_SUBKEL";
	public static final String _NM_SUBKEL = "NM_SUBKEL";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";

	@TableColumn(name = _KD_SUBKEL, primaryKey = true, autoIncrement = true)
	@JsonProperty(_KD_SUBKEL)
	private int kdSubkel;

	@TableColumn(_NM_SUBKEL)
	@JsonProperty(_NM_SUBKEL)
	private String nmSubkel;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	private Date dateUpdate;


	public int getKdSubkel() { return this.kdSubkel; }
	public String getNmSubkel() { return this.nmSubkel; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdSubkel(int kdSubkel) { this.kdSubkel = kdSubkel; }
	public void setNmSubkel(String nmSubkel) { this.nmSubkel = nmSubkel; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}