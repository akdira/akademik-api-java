package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = AbsensiHarianDetailModel.TABLE_NAME)
public class AbsensiHarianDetailModel extends BaseModel {
    public static final String TABLE_NAME = "absensi_harian_detail";
	public static final String _KD_ABSEN_DETAIL = "KD_ABSEN_DETAIL";
	public static final String _KD_ABSEN = "KD_ABSEN";
	public static final String _NIS = "NIS";
	public static final String _STATUS = "STATUS";
	public static final String _KETERANGAN = "KETERANGAN";

	@TableColumn(name = _KD_ABSEN_DETAIL, primaryKey = true, autoIncrement = true)
	@JsonProperty(_KD_ABSEN_DETAIL)
	private int kdAbsenDetail;

	@TableColumn(_KD_ABSEN)
	@JsonProperty(_KD_ABSEN)
	private Integer kdAbsen;

	@TableColumn(_NIS)
	@JsonProperty(_NIS)
	private String nis;

	@TableColumn(_STATUS)
	@JsonProperty(_STATUS)
	private char status;

	@TableColumn(_KETERANGAN)
	@JsonProperty(_KETERANGAN)
	private String keterangan;


	public int getKdAbsenDetail() { return this.kdAbsenDetail; }
	public Integer getKdAbsen() { return this.kdAbsen; }
	public String getNis() { return this.nis; }
	public char getStatus() { return this.status; }
	public String getKeterangan() { return this.keterangan; }

	public void setKdAbsenDetail(int kdAbsenDetail) { this.kdAbsenDetail = kdAbsenDetail; }
	public void setKdAbsen(Integer kdAbsen) { this.kdAbsen = kdAbsen; }
	public void setNis(String nis) { this.nis = nis; }
	public void setStatus(char status) { this.status = status; }
	public void setKeterangan(String keterangan) { this.keterangan = keterangan; }

}