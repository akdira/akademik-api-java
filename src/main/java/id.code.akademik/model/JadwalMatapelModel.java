package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = JadwalMatapelModel.TABLE_NAME)
public class JadwalMatapelModel extends BaseModel {
    public static final String TABLE_NAME = "jadwal_matapel";
	public static final String _KD_JADWAL = "KD_JADWAL";
	public static final String _KD_THN = "KD_THN";
	public static final String _KD_KLS = "KD_KLS";
	public static final String _KD_PEL = "KD_PEL";
	public static final String _NIK = "NIK";
	public static final String _KD_RUANG = "KD_RUANG";
	public static final String _SEM = "SEM";
	public static final String _HARI = "HARI";
	public static final String _JAM_AWAL = "JAM_AWAL";
	public static final String _JAM_AKHIR = "JAM_AKHIR";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";

	@TableColumn(name = _KD_JADWAL, primaryKey = true, autoIncrement = true)
	@JsonProperty(_KD_JADWAL)
	private long kdJadwal;

	@TableColumn(_KD_THN)
	@JsonProperty(_KD_THN)
	private String kdThn;

	@TableColumn(_KD_KLS)
	@JsonProperty(_KD_KLS)
	private String kdKls;

	@TableColumn(_KD_PEL)
	@JsonProperty(_KD_PEL)
	private String kdPel;

	@TableColumn(_NIK)
	@JsonProperty(_NIK)
	private String nik;

	@TableColumn(_KD_RUANG)
	@JsonProperty(_KD_RUANG)
	private String kdRuang;

	@TableColumn(_SEM)
	@JsonProperty(_SEM)
	private char sem;

	@TableColumn(_HARI)
	@JsonProperty(_HARI)
	private String hari;

	@TableColumn(_JAM_AWAL)
	@JsonProperty(_JAM_AWAL)
	private Integer jamAwal;

	@TableColumn(_JAM_AKHIR)
	@JsonProperty(_JAM_AKHIR)
	private Integer jamAkhir;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	private Date dateUpdate;


	public long getKdJadwal() { return this.kdJadwal; }
	public String getKdThn() { return this.kdThn; }
	public String getKdKls() { return this.kdKls; }
	public String getKdPel() { return this.kdPel; }
	public String getNik() { return this.nik; }
	public String getKdRuang() { return this.kdRuang; }
	public char getSem() { return this.sem; }
	public String getHari() { return this.hari; }
	public Integer getJamAwal() { return this.jamAwal; }
	public Integer getJamAkhir() { return this.jamAkhir; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdJadwal(long kdJadwal) { this.kdJadwal = kdJadwal; }
	public void setKdThn(String kdThn) { this.kdThn = kdThn; }
	public void setKdKls(String kdKls) { this.kdKls = kdKls; }
	public void setKdPel(String kdPel) { this.kdPel = kdPel; }
	public void setNik(String nik) { this.nik = nik; }
	public void setKdRuang(String kdRuang) { this.kdRuang = kdRuang; }
	public void setSem(char sem) { this.sem = sem; }
	public void setHari(String hari) { this.hari = hari; }
	public void setJamAwal(Integer jamAwal) { this.jamAwal = jamAwal; }
	public void setJamAkhir(Integer jamAkhir) { this.jamAkhir = jamAkhir; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}