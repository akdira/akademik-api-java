package id.code.akademik.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = ZvUserLoginModel.TABLE_NAME)
public class ZvUserLoginModel extends BaseModel {
	public static final String TABLE_NAME = "zv_user_login";
	public static final String _ACCESS_TOKEN = "access_token";
	public static final String _ID_USER = "ID_USER";
	public static final String _ID_AKSES = "ID_AKSES";
	public static final String _USERNAME = "USERNAME";
	public static final String _PASSWORD = "PASSWORD";
	public static final String _EMAIL = "EMAIL";
	public static final String _PHOTO = "PHOTO";
	public static final String _PHOTO_THUMB = "PHOTO_THUMB";
	public static final String _DATE_LOGIN = "DATE_LOGIN";
	public static final String _DATE_LOGOUT = "DATE_LOGOUT";
	//public static final String _DATE_CREATE = "DATE_CREATE";
	//public static final String _DATE_UPDATE = "DATE_UPDATE";
	public static final String _NIK = "NIK";
	public static final String _NIP = "NIP";
	public static final String _NM_JAB = "NM_JAB";
	public static final String _NAMA = "NAMA";
	public static final String _NM_SISWA = "NM_SISWA";
	public static final String _NM_GURU = "NM_GURU";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";
	public static final String _ACCESS_THNPEL = "ACCESS_THNPEL";
	public static final String _KD_JAB = "KD_JAB";
	public static final String _RESET_PASSWORD = "RESET_PASSWORD";

	@JsonProperty(_ACCESS_TOKEN)
	private String accessToken;

	@TableColumn(name = _ID_USER, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID_USER)
	private int idUser;

	@TableColumn(_ID_AKSES)
	@JsonProperty(_ID_AKSES)
	@ValidateColumn(_ID_AKSES)
	private int idAkses;

	@TableColumn(_USERNAME)
	@JsonProperty(_USERNAME)
	@ValidateColumn(_USERNAME)
	private String username;

	@TableColumn(_PASSWORD)
	@JsonProperty(value = _PASSWORD, access = JsonProperty.Access.WRITE_ONLY /* don't show password to client! */)
	private String password;

	@TableColumn(_EMAIL)
	@JsonProperty(_EMAIL)
	@ValidateColumn(_EMAIL)
	private String email;

	@TableColumn(_PHOTO)
	@JsonProperty(_PHOTO)
	//@ValidateColumn(_PHOTO)
	private String photo;

	@TableColumn(_PHOTO_THUMB)
	@JsonProperty(_PHOTO_THUMB)
	private String photoThumb;

	@TableColumn(_DATE_LOGIN)
	@JsonProperty(_DATE_LOGIN)
	//@ValidateColumn(_DATE_LOGIN)
	private Date dateLogin = new SimpleDateFormat("yyyy-MM-dd").parse("2018-07-25");

	@TableColumn(_DATE_LOGOUT)
	@JsonProperty(_DATE_LOGOUT)
	//@ValidateColumn(_DATE_LOGOUT)
	private Date dateLogout = new SimpleDateFormat("yyyy-MM-dd").parse("2018-07-25");

	@TableColumn(_NIK)
	@JsonProperty(_NIK)
	private String nik;

	@TableColumn(_NIP)
	@JsonProperty(_NIP)
	private String nip;

	@TableColumn(_NM_JAB)
	@JsonProperty(_NM_JAB)
	private String nmJab;

	@TableColumn(_NAMA)
	@JsonProperty(_NAMA)
	private String nama;

	@TableColumn(_NM_SISWA)
	@JsonProperty(_NM_SISWA)
	private String nmSiswa;

	@TableColumn(_NM_GURU)
	@JsonProperty(_NM_GURU)
	private String nmGuru;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	@ValidateColumn(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	@ValidateColumn(_DATE_UPDATE)
	private Date dateUpdate;

	@TableColumn(_ACCESS_THNPEL)
	@JsonProperty(_ACCESS_THNPEL)
	//@ValidateColumn(_ACCESS_THNPEL)
	private String accessThnpel;

	@TableColumn(_KD_JAB)
	@JsonProperty(_KD_JAB)
	private String kdJab;

	@TableColumn(_RESET_PASSWORD)
	@JsonProperty(_RESET_PASSWORD)
	private String resetPassword;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public int getIdAkses() {
		return idAkses;
	}

	public void setIdAkses(int idAkses) {
		this.idAkses = idAkses;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getPhotoThumb() {
		return photoThumb;
	}

	public void setPhotoThumb(String photoThumb) {
		this.photoThumb = photoThumb;
	}

	public Date getDateLogin() {
		return dateLogin;
	}

	public void setDateLogin(Date dateLogin) {
		this.dateLogin = dateLogin;
	}

	public Date getDateLogout() {
		return dateLogout;
	}

	public void setDateLogout(Date dateLogout) {
		this.dateLogout = dateLogout;
	}

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getNmJab() {
		return nmJab;
	}

	public void setNmJab(String nmJab) {
		this.nmJab = nmJab;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getNmSiswa() {
		return nmSiswa;
	}

	public void setNmSiswa(String nmSiswa) {
		this.nmSiswa = nmSiswa;
	}

	public String getNmGuru() {
		return nmGuru;
	}

	public void setNmGuru(String nmGuru) {
		this.nmGuru = nmGuru;
	}

	public Date getDateCreate() {
		return dateCreate;
	}

	public void setDateCreate(Date dateCreate) {
		this.dateCreate = dateCreate;
	}

	public Date getDateUpdate() {
		return dateUpdate;
	}

	public void setDateUpdate(Date dateUpdate) {
		this.dateUpdate = dateUpdate;
	}

	public String getAccessThnpel() {
		return accessThnpel;
	}

	public void setAccessThnpel(String accessThnpel) {
		this.accessThnpel = accessThnpel;
	}

	public String getKdJab() {
		return kdJab;
	}

	public void setKdJab(String kdJab) {
		this.kdJab = kdJab;
	}

	public String getResetPassword() {
		return resetPassword;
	}

	public void setResetPassword(String resetPassword) {
		this.resetPassword = resetPassword;
	}

	public ZvUserLoginModel() throws ParseException {
	}

}