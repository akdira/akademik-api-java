package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = PenilaianModel.TABLE_NAME)
public class PenilaianModel extends BaseModel {
    public static final String TABLE_NAME = "penilaian";
	public static final String _KD_NILAI = "KD_NILAI";
	public static final String _KD_THN = "KD_THN";
	public static final String _KD_KLS = "KD_KLS";
	public static final String _KD_PEL = "KD_PEL";
	public static final String _NIK = "NIK";
	public static final String _SEM = "SEM";
	public static final String _TYPE = "TYPE";
	public static final String _KKM = "KKM";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";

	@TableColumn(name = _KD_NILAI, primaryKey = true, autoIncrement = true)
	@JsonProperty(_KD_NILAI)
	private int kdNilai;

	@TableColumn(_KD_THN)
	@JsonProperty(_KD_THN)
	@ValidateColumn(_KD_THN)
	private String kdThn;

	@TableColumn(_KD_KLS)
	@JsonProperty(_KD_KLS)
	@ValidateColumn(_KD_KLS)
	private String kdKls;

	@TableColumn(_KD_PEL)
	@JsonProperty(_KD_PEL)
	@ValidateColumn(_KD_PEL)
	private String kdPel;

	@TableColumn(_NIK)
	@JsonProperty(_NIK)
	@ValidateColumn(_NIK)
	private String nik;

	@TableColumn(_SEM)
	@JsonProperty(_SEM)
	@ValidateColumn(_SEM)
	private int sem;

	@TableColumn(_TYPE)
	@JsonProperty(_TYPE)
	@ValidateColumn(_TYPE)
	private String type;

	@TableColumn(_KKM)
	@JsonProperty(_KKM)
	private Double kkm;

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	private Date dateUpdate;


	public int getKdNilai() { return this.kdNilai; }
	public String getKdThn() { return this.kdThn; }
	public String getKdKls() { return this.kdKls; }
	public String getKdPel() { return this.kdPel; }
	public String getNik() { return this.nik; }
	public int getSem() { return this.sem; }
	public String getType() { return this.type; }
	public Double getKkm() { return this.kkm; }
	public Date getDateCreate() { return this.dateCreate; }
	public Date getDateUpdate() { return this.dateUpdate; }

	public void setKdNilai(int kdNilai) { this.kdNilai = kdNilai; }
	public void setKdThn(String kdThn) { this.kdThn = kdThn; }
	public void setKdKls(String kdKls) { this.kdKls = kdKls; }
	public void setKdPel(String kdPel) { this.kdPel = kdPel; }
	public void setNik(String nik) { this.nik = nik; }
	public void setSem(int sem) { this.sem = sem; }
	public void setType(String type) { this.type = type; }
	public void setKkm(Double kkm) { this.kkm = kkm; }
	public void setDateCreate(Date dateCreate) { this.dateCreate = dateCreate; }
	public void setDateUpdate(Date dateUpdate) { this.dateUpdate = dateUpdate; }

}