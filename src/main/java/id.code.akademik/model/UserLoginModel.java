package id.code.akademik.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 19/07/2018.
 */

@Table(name = UserLoginModel.TABLE_NAME)
public class 	UserLoginModel extends BaseModel {
	public static final String TABLE_NAME = "user_login";
	public static final String _ACCESS_TOKEN = "access_token";
	public static final String _ID_USER = "ID_USER";
	public static final String _ID_AKSES = "ID_AKSES";
	public static final String _USERNAME = "USERNAME";
	public static final String _PASSWORD = "PASSWORD";
	public static final String _EMAIL = "EMAIL";
	public static final String _PHOTO = "PHOTO";
	public static final String _DATE_LOGIN = "DATE_LOGIN";
	public static final String _DATE_LOGOUT = "DATE_LOGOUT";
	public static final String _DATE_CREATE = "DATE_CREATE";
	public static final String _DATE_UPDATE = "DATE_UPDATE";
	public static final String _ACCESS_THNPEL = "ACCESS_THNPEL";
	public static final String _PHOTO_THUMB = "PHOTO_THUMB";
	public static final String _RESET_PASSWORD = "RESET_PASSWORD";

	@JsonProperty(_ACCESS_TOKEN)
	private String accessToken;

	@TableColumn(name = _ID_USER, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID_USER)
	private int idUser;

	@TableColumn(_ID_AKSES)
	@JsonProperty(_ID_AKSES)
	@ValidateColumn(_ID_AKSES)
	private int idAkses;

	@TableColumn(_USERNAME)
	@JsonProperty(_USERNAME)
	@ValidateColumn(_USERNAME)
	private String username;

	@TableColumn(_PASSWORD)
	@JsonProperty(value = _PASSWORD, access = JsonProperty.Access.WRITE_ONLY /* don't show password to client! */)
	private String password;

	@TableColumn(_EMAIL)
	@JsonProperty(_EMAIL)
	@ValidateColumn(_EMAIL)
	private String email;

	@TableColumn(_PHOTO)
	@JsonProperty(_PHOTO)
	//@ValidateColumn(_PHOTO)
	private String photo;

	@TableColumn(_DATE_LOGIN)
	@JsonProperty(_DATE_LOGIN)
	//@ValidateColumn(_DATE_LOGIN)
	private Date dateLogin = new SimpleDateFormat("yyyy-MM-dd").parse("2018-07-25");

	@TableColumn(_DATE_LOGOUT)
	@JsonProperty(_DATE_LOGOUT)
	//@ValidateColumn(_DATE_LOGOUT)
	private Date dateLogout = new SimpleDateFormat("yyyy-MM-dd").parse("2018-07-25");

	@TableColumn(_DATE_CREATE)
	@JsonProperty(_DATE_CREATE)
	@ValidateColumn(_DATE_CREATE)
	private Date dateCreate;

	@TableColumn(_DATE_UPDATE)
	@JsonProperty(_DATE_UPDATE)
	@ValidateColumn(_DATE_UPDATE)
	private Date dateUpdate;

	@TableColumn(_ACCESS_THNPEL)
	@JsonProperty(_ACCESS_THNPEL)
	//@ValidateColumn(_ACCESS_THNPEL)
	private String accessThnpel;

	@TableColumn(_PHOTO_THUMB)
	@JsonProperty(_PHOTO_THUMB)
	private String photoThumb;

	@TableColumn(_RESET_PASSWORD)
	@JsonProperty(_RESET_PASSWORD)
	private String resetPassword;

	public String getAccessToken() {
		return this.accessToken;
	}

	public int getIdUser() {
		return this.idUser;
	}

	public int getIdAkses() {
		return this.idAkses;
	}

	public String getUsername() {
		return this.username;
	}

	public String getPassword() {
		return this.password;
	}

	public String getEmail() {
		return this.email;
	}

	public String getPhoto() {
		return this.photo;
	}

	public Date getDateLogin() {
		return this.dateLogin;
	}

	public Date getDateLogout() {
		return this.dateLogout;
	}

	public Date getDateCreate() {
		return this.dateCreate;
	}

	public Date getDateUpdate() {
		return this.dateUpdate;
	}

	public String getAccessThnpel() {
		return this.accessThnpel;
	}

	public String getPhotoThumb() {
		return this.photoThumb;
	}

	public String getResetPassword() {
		return this.resetPassword;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public void setIdAkses(int idAkses) {
		this.idAkses = idAkses;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public void setDateLogin(Date dateLogin) {
		this.dateLogin = dateLogin;
	}

	public void setDateLogout(Date dateLogout) {
		this.dateLogout = dateLogout;
	}

	public void setDateCreate(Date dateCreate) {
		this.dateCreate = dateCreate;
	}

	public void setDateUpdate(Date dateUpdate) {
		this.dateUpdate = dateUpdate;
	}

	public void setAccessThnpel(String accessThnpel) {
		this.accessThnpel = accessThnpel;
	}

	public void setPhotoThumb(String photoThumb) {
		this.photoThumb = photoThumb;
	}

	public void setResetPassword(String resetPassword) {
		this.resetPassword = resetPassword;
	}

	public UserLoginModel() throws ParseException {
	}

}