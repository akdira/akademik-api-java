package id.code.akademik.api;

import id.code.server.ApiHandler;
import id.code.akademik.api.handler.*;


import java.util.Map;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by Web Api Generator 19/07/2018.
 */
public class Route {
    public static Map<String, ApiHandler> createRoutes(Properties properties) throws Exception {
        final Map<String, ApiHandler> routes = new HashMap<>();

		routes.put("login", new UserLoginLoginHandler());
		routes.put("register", new UserLoginRegisterHandler());
		routes.put("change_password", new UserLoginChangePasswordHandler());

		routes.put("hak_akses", new HakAksesHandler());
		routes.put("user_login_log", new UserLoginLogHandler());
		routes.put("rpp", new RppHandler());
		routes.put("kelas", new KelasHandler());
		routes.put("waktu_jadwal", new WaktuJadwalHandler());
		routes.put("sub_kelompok", new SubKelompokHandler());
		routes.put("siswa", new SiswaHandler());
		routes.put("profile_sekolah", new ProfileSekolahHandler());
		routes.put("rombongan_belajar", new RombonganBelajarHandler());
		routes.put("menus", new MenusHandler());
		routes.put("mata_pelajaran", new MataPelajaranHandler());
		routes.put("ruangan", new RuanganHandler());
		routes.put("jabatan", new JabatanHandler());
		routes.put("kelompok", new KelompokHandler());
		routes.put("jadwal_matapel", new JadwalMatapelHandler());
		routes.put("penilaian_detail", new PenilaianDetailHandler());
		routes.put("guru", new GuruHandler());
		routes.put("jurnal_detail", new JurnalDetailHandler());
		routes.put("jurnal", new JurnalHandler());
		routes.put("penilaian", new PenilaianHandler());
		routes.put("tingkat_kelas", new TingkatKelasHandler());
		routes.put("jurusan", new JurusanHandler());
		routes.put("user_login", new UserLoginHandler());
		routes.put("zv_user_login", new ZvUserLoginHandler());
		routes.put("hari", new HariHandler());
		routes.put("absensi_harian_detail", new AbsensiHarianDetailHandler());
		routes.put("guru_piket", new GuruPiketHandler());
		routes.put("tahun_pelajaran", new TahunPelajaranHandler());
		routes.put("absensi_harian", new AbsensiHarianHandler());
		routes.put("wali_kelas", new WaliKelasHandler());

        return routes;
    }
}