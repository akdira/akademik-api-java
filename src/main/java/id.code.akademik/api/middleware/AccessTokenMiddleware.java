package id.code.akademik.api.middleware;

import id.code.server.AccessTokenValidator;
import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.UserLoginFacade;
import id.code.akademik.model.UserLoginModel;

import java.util.Map;
import java.util.Properties;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class AccessTokenMiddleware extends AccessTokenValidator<AccessTokenPayload> {
    private final UserLoginFacade facade = new UserLoginFacade();

    public AccessTokenMiddleware(Properties properties) {
        super(properties);
    }

    @Override
    public AccessTokenPayload getRequestedClaim(Map payloadFromClient) throws Exception {
        return new AccessTokenPayload(payloadFromClient);
    }

    @Override
    public AccessTokenPayload getValidatedClaim(AccessTokenPayload payload) throws Exception {
        final UserLoginModel payloadFromDatabase = this.facade.findByIdUser(payload.getIdUser());
        return payloadFromDatabase == null ? null : new AccessTokenPayload(payloadFromDatabase);
    }
}