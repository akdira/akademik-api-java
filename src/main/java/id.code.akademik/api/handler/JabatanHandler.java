package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.JabatanFacade;
import id.code.akademik.filter.JabatanFilter;
import id.code.akademik.model.JabatanModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class JabatanHandler extends RouteApiHandler<AccessTokenPayload> {
    private final JabatanFacade jabatanFacade = new JabatanFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<JabatanFilter> filter) throws Exception {
        final List<JabatanModel> items = this.jabatanFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{kdJab}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, int kdJab) throws Exception {
        final JabatanModel data = this.jabatanFacade.findByKdJab(kdJab);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody JabatanModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.jabatanFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{kdJab}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, int kdJab) throws Exception {
        final boolean deleted = this.jabatanFacade.delete(kdJab);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{kdJab}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody JabatanModel newData, Integer kdJab) throws Exception {
        newData.setKdJab(kdJab);

        if (this.jabatanFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}