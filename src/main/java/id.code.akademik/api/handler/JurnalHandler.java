package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.JurnalFacade;
import id.code.akademik.filter.JurnalFilter;
import id.code.akademik.model.JurnalModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class JurnalHandler extends RouteApiHandler<AccessTokenPayload> {
    private final JurnalFacade jurnalFacade = new JurnalFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<JurnalFilter> filter) throws Exception {
        final List<JurnalModel> items = this.jurnalFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{kdJurnal}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, int kdJurnal) throws Exception {
        final JurnalModel data = this.jurnalFacade.findByKdJurnal(kdJurnal);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody JurnalModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.jurnalFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{kdJurnal}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, int kdJurnal) throws Exception {
        final boolean deleted = this.jurnalFacade.delete(kdJurnal);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{kdJurnal}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody JurnalModel newData, Integer kdJurnal) throws Exception {
        newData.setKdJurnal(kdJurnal);

        if (this.jurnalFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}