package id.code.akademik.api.handler;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.akademik.validation.UpdateUserLoginValidation;
import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.UserLoginFacade;
import id.code.akademik.filter.UserLoginFilter;
import id.code.akademik.model.UserLoginModel;
import org.apache.logging.log4j.core.jmx.Server;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class UserLoginHandler extends RouteApiHandler<AccessTokenPayload> {
    private final UserLoginFacade userLoginFacade = new UserLoginFacade();

    //@Override
    //protected boolean authorizeAccess(ServerExchange<AccessTokenPayload> exchange)
    //{
    //    return !exchange.getRequestMethod().equals(HTTP_GET);
    //}

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<UserLoginFilter> filter) throws Exception {
        final List<UserLoginModel> items = this.userLoginFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{idUser}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, int idUser) throws Exception {
        final UserLoginModel data = this.userLoginFacade.findByIdUser(idUser);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    //@HandlerPost
    @HandlerPost(authorizeAccess = false, bypassAllMiddleware = true)
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody UpdateUserLoginValidation newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.userLoginFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{idUser}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, int idUser) throws Exception {
        final boolean deleted = this.userLoginFacade.delete(idUser);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{idUser}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody UserLoginModel newData, Integer idUser) throws Exception {
        newData.setIdUser(idUser);

        if (this.userLoginFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}