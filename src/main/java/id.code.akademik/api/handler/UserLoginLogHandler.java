package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.UserLoginLogFacade;
import id.code.akademik.filter.UserLoginLogFilter;
import id.code.akademik.model.UserLoginLogModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class UserLoginLogHandler extends RouteApiHandler<AccessTokenPayload> {
    private final UserLoginLogFacade userLoginLogFacade = new UserLoginLogFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<UserLoginLogFilter> filter) throws Exception {
        final List<UserLoginLogModel> items = this.userLoginLogFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{idUll}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, long idUll) throws Exception {
        final UserLoginLogModel data = this.userLoginLogFacade.findByIdUll(idUll);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody UserLoginLogModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.userLoginLogFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{idUll}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, long idUll) throws Exception {
        final boolean deleted = this.userLoginLogFacade.delete(idUll);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{idUll}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody UserLoginLogModel newData, Long idUll) throws Exception {
        newData.setIdUll(idUll);

        if (this.userLoginLogFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}