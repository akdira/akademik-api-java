package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.KelompokFacade;
import id.code.akademik.filter.KelompokFilter;
import id.code.akademik.model.KelompokModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class KelompokHandler extends RouteApiHandler<AccessTokenPayload> {
    private final KelompokFacade kelompokFacade = new KelompokFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<KelompokFilter> filter) throws Exception {
        final List<KelompokModel> items = this.kelompokFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{kdKel}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, int kdKel) throws Exception {
        final KelompokModel data = this.kelompokFacade.findByKdKel(kdKel);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody KelompokModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.kelompokFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{kdKel}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, int kdKel) throws Exception {
        final boolean deleted = this.kelompokFacade.delete(kdKel);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{kdKel}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody KelompokModel newData, Integer kdKel) throws Exception {
        newData.setKdKel(kdKel);

        if (this.kelompokFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}