package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.ProfileSekolahFacade;
import id.code.akademik.filter.ProfileSekolahFilter;
import id.code.akademik.model.ProfileSekolahModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class ProfileSekolahHandler extends RouteApiHandler<AccessTokenPayload> {
    private final ProfileSekolahFacade profileSekolahFacade = new ProfileSekolahFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<ProfileSekolahFilter> filter) throws Exception {
        final List<ProfileSekolahModel> items = this.profileSekolahFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{idSekolah}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, int idSekolah) throws Exception {
        final ProfileSekolahModel data = this.profileSekolahFacade.findByIdSekolah(idSekolah);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody ProfileSekolahModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.profileSekolahFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{idSekolah}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, int idSekolah) throws Exception {
        final boolean deleted = this.profileSekolahFacade.delete(idSekolah);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{idSekolah}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody ProfileSekolahModel newData, Integer idSekolah) throws Exception {
        newData.setIdSekolah(idSekolah);

        if (this.profileSekolahFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}