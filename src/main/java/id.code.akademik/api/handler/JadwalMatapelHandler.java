package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.JadwalMatapelFacade;
import id.code.akademik.filter.JadwalMatapelFilter;
import id.code.akademik.model.JadwalMatapelModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class JadwalMatapelHandler extends RouteApiHandler<AccessTokenPayload> {
    private final JadwalMatapelFacade jadwalMatapelFacade = new JadwalMatapelFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<JadwalMatapelFilter> filter) throws Exception {
        final List<JadwalMatapelModel> items = this.jadwalMatapelFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{kdJadwal}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, long kdJadwal) throws Exception {
        final JadwalMatapelModel data = this.jadwalMatapelFacade.findByKdJadwal(kdJadwal);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody JadwalMatapelModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.jadwalMatapelFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{kdJadwal}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, long kdJadwal) throws Exception {
        final boolean deleted = this.jadwalMatapelFacade.delete(kdJadwal);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{kdJadwal}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody JadwalMatapelModel newData, Long kdJadwal) throws Exception {
        newData.setKdJadwal(kdJadwal);

        if (this.jadwalMatapelFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}