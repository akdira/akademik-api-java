package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.PenilaianFacade;
import id.code.akademik.filter.PenilaianFilter;
import id.code.akademik.model.PenilaianModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class PenilaianHandler extends RouteApiHandler<AccessTokenPayload> {
    private final PenilaianFacade penilaianFacade = new PenilaianFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<PenilaianFilter> filter) throws Exception {
        final List<PenilaianModel> items = this.penilaianFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{kdNilai}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, int kdNilai) throws Exception {
        final PenilaianModel data = this.penilaianFacade.findByKdNilai(kdNilai);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody PenilaianModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.penilaianFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{kdNilai}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, int kdNilai) throws Exception {
        final boolean deleted = this.penilaianFacade.delete(kdNilai);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{kdNilai}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody PenilaianModel newData, Integer kdNilai) throws Exception {
        newData.setKdNilai(kdNilai);

        if (this.penilaianFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}