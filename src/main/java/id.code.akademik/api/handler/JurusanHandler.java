package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.JurusanFacade;
import id.code.akademik.filter.JurusanFilter;
import id.code.akademik.model.JurusanModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class JurusanHandler extends RouteApiHandler<AccessTokenPayload> {
    private final JurusanFacade jurusanFacade = new JurusanFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<JurusanFilter> filter) throws Exception {
        final List<JurusanModel> items = this.jurusanFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{kdJur}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, String kdJur) throws Exception {
        final JurusanModel data = this.jurusanFacade.findByKdJur(kdJur);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody JurusanModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.jurusanFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{kdJur}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, String kdJur) throws Exception {
        final boolean deleted = this.jurusanFacade.delete(kdJur);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{kdJur}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody JurusanModel newData, String kdJur) throws Exception {
        newData.setKdJur(kdJur);

        if (this.jurusanFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}