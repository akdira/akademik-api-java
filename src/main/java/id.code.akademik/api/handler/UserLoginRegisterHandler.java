package id.code.akademik.api.handler;

import id.code.server.ServerExchange;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.UserLoginFacade;
import id.code.akademik.model.UserLoginModel;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class UserLoginRegisterHandler extends RouteApiHandler<AccessTokenPayload> {
    public static final ApiResponse RESPONSE_DUPLICATE = new ApiResponse(STATUS_CONFLICT,  "Duplicate username");
    private UserLoginFacade userLoginFacade = new UserLoginFacade();

    @HandlerPost(authorizeAccess = false, bypassAllMiddleware = true)
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody UserLoginModel newData) throws Exception {
        if (!this.userLoginFacade.isUsernameAvailable(newData.getUsername())) {
            super.sendResponse(exchange, RESPONSE_DUPLICATE);
        } else {
            this.userLoginFacade.insert(newData);
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }
}