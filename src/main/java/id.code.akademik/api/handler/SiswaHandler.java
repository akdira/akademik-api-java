package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.SiswaFacade;
import id.code.akademik.filter.SiswaFilter;
import id.code.akademik.model.SiswaModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class SiswaHandler extends RouteApiHandler<AccessTokenPayload> {
    private final SiswaFacade siswaFacade = new SiswaFacade();

    @HandlerGet(authorizeAccess = false)
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<SiswaFilter> filter) throws Exception {
        final List<SiswaModel> items = this.siswaFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{nis}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, String nis) throws Exception {
        final SiswaModel data = this.siswaFacade.findByNis(nis);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody SiswaModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.siswaFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{nis}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, String nis) throws Exception {
        final boolean deleted = this.siswaFacade.delete(nis);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{nis}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody SiswaModel newData, String nis) throws Exception {
        newData.setNis(nis);

        if (this.siswaFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}