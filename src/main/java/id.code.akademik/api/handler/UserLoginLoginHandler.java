package id.code.akademik.api.handler;

import id.code.server.ServerExchange;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.ApiResponse;

import id.code.akademik.validation.LoginValidation;
import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.UserLoginFacade;
import id.code.akademik.model.UserLoginModel;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class UserLoginLoginHandler extends RouteApiHandler<AccessTokenPayload> {
    public static final ApiResponse RESPONSE_INVALID_USER_LOGIN = new ApiResponse(STATUS_INVALID_LOGIN,  "Invalid username or password");
    private UserLoginFacade userLoginFacade = new UserLoginFacade();

    @HandlerPost(authorizeAccess = false, bypassAllMiddleware = true)
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody LoginValidation login) throws Exception {
        final UserLoginModel model = this.userLoginFacade.login(login.getUsername(), login.getPassword());

        if (model == null) {
            super.sendResponse(exchange, RESPONSE_INVALID_USER_LOGIN);
        } else {
            final AccessTokenPayload payload = new AccessTokenPayload(model);
            model.setAccessToken(super.getAccessTokenValidator().generateAccessToken(payload));
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, model));
        }
    }
}