package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.WaktuJadwalFacade;
import id.code.akademik.filter.WaktuJadwalFilter;
import id.code.akademik.model.WaktuJadwalModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class WaktuJadwalHandler extends RouteApiHandler<AccessTokenPayload> {
    private final WaktuJadwalFacade waktuJadwalFacade = new WaktuJadwalFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<WaktuJadwalFilter> filter) throws Exception {
        final List<WaktuJadwalModel> items = this.waktuJadwalFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{kdWaktu}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, String kdWaktu) throws Exception {
        final WaktuJadwalModel data = this.waktuJadwalFacade.findByKdWaktu(kdWaktu);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody WaktuJadwalModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.waktuJadwalFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{kdWaktu}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, String kdWaktu) throws Exception {
        final boolean deleted = this.waktuJadwalFacade.delete(kdWaktu);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{kdWaktu}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody WaktuJadwalModel newData, String kdWaktu) throws Exception {
        newData.setKdWaktu(kdWaktu);

        if (this.waktuJadwalFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}