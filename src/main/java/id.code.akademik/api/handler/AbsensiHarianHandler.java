package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.AbsensiHarianFacade;
import id.code.akademik.filter.AbsensiHarianFilter;
import id.code.akademik.model.AbsensiHarianModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class AbsensiHarianHandler extends RouteApiHandler<AccessTokenPayload> {
    private final AbsensiHarianFacade absensiHarianFacade = new AbsensiHarianFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<AbsensiHarianFilter> filter) throws Exception {
        final List<AbsensiHarianModel> items = this.absensiHarianFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{kdAbsen}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, int kdAbsen) throws Exception {
        final AbsensiHarianModel data = this.absensiHarianFacade.findByKdAbsen(kdAbsen);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody AbsensiHarianModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.absensiHarianFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{kdAbsen}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, int kdAbsen) throws Exception {
        final boolean deleted = this.absensiHarianFacade.delete(kdAbsen);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{kdAbsen}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody AbsensiHarianModel newData, Integer kdAbsen) throws Exception {
        newData.setKdAbsen(kdAbsen);

        if (this.absensiHarianFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}