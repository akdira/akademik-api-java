package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.JurnalDetailFacade;
import id.code.akademik.filter.JurnalDetailFilter;
import id.code.akademik.model.JurnalDetailModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class JurnalDetailHandler extends RouteApiHandler<AccessTokenPayload> {
    private final JurnalDetailFacade jurnalDetailFacade = new JurnalDetailFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<JurnalDetailFilter> filter) throws Exception {
        final List<JurnalDetailModel> items = this.jurnalDetailFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{kdJurnalDetail}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, int kdJurnalDetail) throws Exception {
        final JurnalDetailModel data = this.jurnalDetailFacade.findByKdJurnalDetail(kdJurnalDetail);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody JurnalDetailModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.jurnalDetailFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{kdJurnalDetail}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, int kdJurnalDetail) throws Exception {
        final boolean deleted = this.jurnalDetailFacade.delete(kdJurnalDetail);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{kdJurnalDetail}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody JurnalDetailModel newData, Integer kdJurnalDetail) throws Exception {
        newData.setKdJurnalDetail(kdJurnalDetail);

        if (this.jurnalDetailFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}