package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.RuanganFacade;
import id.code.akademik.filter.RuanganFilter;
import id.code.akademik.model.RuanganModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class RuanganHandler extends RouteApiHandler<AccessTokenPayload> {
    private final RuanganFacade ruanganFacade = new RuanganFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<RuanganFilter> filter) throws Exception {
        final List<RuanganModel> items = this.ruanganFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{kdRuang}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, String kdRuang) throws Exception {
        final RuanganModel data = this.ruanganFacade.findByKdRuang(kdRuang);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody RuanganModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.ruanganFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{kdRuang}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, String kdRuang) throws Exception {
        final boolean deleted = this.ruanganFacade.delete(kdRuang);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{kdRuang}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody RuanganModel newData, String kdRuang) throws Exception {
        newData.setKdRuang(kdRuang);

        if (this.ruanganFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}