package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.GuruPiketFacade;
import id.code.akademik.filter.GuruPiketFilter;
import id.code.akademik.model.GuruPiketModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class GuruPiketHandler extends RouteApiHandler<AccessTokenPayload> {
    private final GuruPiketFacade guruPiketFacade = new GuruPiketFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<GuruPiketFilter> filter) throws Exception {
        final List<GuruPiketModel> items = this.guruPiketFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{kdPiket}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, int kdPiket) throws Exception {
        final GuruPiketModel data = this.guruPiketFacade.findByKdPiket(kdPiket);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody GuruPiketModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.guruPiketFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{kdPiket}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, int kdPiket) throws Exception {
        final boolean deleted = this.guruPiketFacade.delete(kdPiket);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{kdPiket}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody GuruPiketModel newData, Integer kdPiket) throws Exception {
        newData.setKdPiket(kdPiket);

        if (this.guruPiketFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}