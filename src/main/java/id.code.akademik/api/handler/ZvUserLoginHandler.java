package id.code.akademik.api.handler;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.ZvUserLoginFacade;
import id.code.akademik.filter.UserLoginFilter;
import id.code.akademik.model.UserLoginModel;
import id.code.akademik.model.ZvUserLoginModel;
import id.code.akademik.validation.UpdateUserLoginValidation;
import id.code.database.filter.Filter;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class ZvUserLoginHandler extends RouteApiHandler<AccessTokenPayload> {
    private final ZvUserLoginFacade zvUserLoginFacade = new ZvUserLoginFacade();

    //@Override
    //protected boolean authorizeAccess(ServerExchange<AccessTokenPayload> exchange)
    //{
    //    return !exchange.getRequestMethod().equals(HTTP_GET);
    //}

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<UserLoginFilter> filter) throws Exception {
        final List<ZvUserLoginModel> items = this.zvUserLoginFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{idUser}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, int idUser) throws Exception {
        final ZvUserLoginModel data = this.zvUserLoginFacade.findByIdUser(idUser);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }


}