package id.code.akademik.api.handler;

import id.code.component.PasswordHasher;
import id.code.server.ServerExchange;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.ApiResponse;

import id.code.akademik.validation.ChangePasswordValidation;
import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.UserLoginFacade;
import id.code.akademik.model.UserLoginModel;

import static id.code.akademik.api.handler.UserLoginLoginHandler.RESPONSE_INVALID_USER_LOGIN;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class UserLoginChangePasswordHandler extends RouteApiHandler<AccessTokenPayload> {
    private UserLoginFacade userLoginFacade = new UserLoginFacade();

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody ChangePasswordValidation value) throws Exception {
        final UserLoginModel model = this.userLoginFacade.findByIdUser(exchange.getAccessTokenPayload().getIdUser());
        if (model == null || !PasswordHasher.verify(value.getOldPassword(), model.getPassword())) {
            super.sendResponse(exchange, RESPONSE_INVALID_USER_LOGIN);
        } else {
            this.userLoginFacade.changePassword(model, value.getNewPassword());
        }
    }
}