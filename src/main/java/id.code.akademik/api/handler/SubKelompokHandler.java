package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.SubKelompokFacade;
import id.code.akademik.filter.SubKelompokFilter;
import id.code.akademik.model.SubKelompokModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class SubKelompokHandler extends RouteApiHandler<AccessTokenPayload> {
    private final SubKelompokFacade subKelompokFacade = new SubKelompokFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<SubKelompokFilter> filter) throws Exception {
        final List<SubKelompokModel> items = this.subKelompokFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{kdSubkel}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, int kdSubkel) throws Exception {
        final SubKelompokModel data = this.subKelompokFacade.findByKdSubkel(kdSubkel);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody SubKelompokModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.subKelompokFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{kdSubkel}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, int kdSubkel) throws Exception {
        final boolean deleted = this.subKelompokFacade.delete(kdSubkel);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{kdSubkel}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody SubKelompokModel newData, Integer kdSubkel) throws Exception {
        newData.setKdSubkel(kdSubkel);

        if (this.subKelompokFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}