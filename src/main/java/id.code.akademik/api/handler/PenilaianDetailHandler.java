package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.PenilaianDetailFacade;
import id.code.akademik.filter.PenilaianDetailFilter;
import id.code.akademik.model.PenilaianDetailModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class PenilaianDetailHandler extends RouteApiHandler<AccessTokenPayload> {
    private final PenilaianDetailFacade penilaianDetailFacade = new PenilaianDetailFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<PenilaianDetailFilter> filter) throws Exception {
        final List<PenilaianDetailModel> items = this.penilaianDetailFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{kdNilaiDetail}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, int kdNilaiDetail) throws Exception {
        final PenilaianDetailModel data = this.penilaianDetailFacade.findByKdNilaiDetail(kdNilaiDetail);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody PenilaianDetailModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.penilaianDetailFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{kdNilaiDetail}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, int kdNilaiDetail) throws Exception {
        final boolean deleted = this.penilaianDetailFacade.delete(kdNilaiDetail);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{kdNilaiDetail}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody PenilaianDetailModel newData, Integer kdNilaiDetail) throws Exception {
        newData.setKdNilaiDetail(kdNilaiDetail);

        if (this.penilaianDetailFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}