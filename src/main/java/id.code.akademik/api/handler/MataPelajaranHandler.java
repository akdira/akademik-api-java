package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.MataPelajaranFacade;
import id.code.akademik.filter.MataPelajaranFilter;
import id.code.akademik.model.MataPelajaranModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class MataPelajaranHandler extends RouteApiHandler<AccessTokenPayload> {
    private final MataPelajaranFacade mataPelajaranFacade = new MataPelajaranFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<MataPelajaranFilter> filter) throws Exception {
        final List<MataPelajaranModel> items = this.mataPelajaranFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{kdPel}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, String kdPel) throws Exception {
        final MataPelajaranModel data = this.mataPelajaranFacade.findByKdPel(kdPel);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody MataPelajaranModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.mataPelajaranFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{kdPel}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, String kdPel) throws Exception {
        final boolean deleted = this.mataPelajaranFacade.delete(kdPel);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{kdPel}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody MataPelajaranModel newData, String kdPel) throws Exception {
        newData.setKdPel(kdPel);

        if (this.mataPelajaranFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}