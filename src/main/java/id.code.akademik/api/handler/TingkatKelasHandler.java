package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.TingkatKelasFacade;
import id.code.akademik.filter.TingkatKelasFilter;
import id.code.akademik.model.TingkatKelasModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class TingkatKelasHandler extends RouteApiHandler<AccessTokenPayload> {
    private final TingkatKelasFacade tingkatKelasFacade = new TingkatKelasFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<TingkatKelasFilter> filter) throws Exception {
        final List<TingkatKelasModel> items = this.tingkatKelasFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{idTingkatKls}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, String idTingkatKls) throws Exception {
        final TingkatKelasModel data = this.tingkatKelasFacade.findByIdTingkatKls(idTingkatKls);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody TingkatKelasModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.tingkatKelasFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{idTingkatKls}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, String idTingkatKls) throws Exception {
        final boolean deleted = this.tingkatKelasFacade.delete(idTingkatKls);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{idTingkatKls}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody TingkatKelasModel newData, String idTingkatKls) throws Exception {
        newData.setIdTingkatKls(idTingkatKls);

        if (this.tingkatKelasFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}