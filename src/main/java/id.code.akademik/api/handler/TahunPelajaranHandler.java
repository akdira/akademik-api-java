package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.TahunPelajaranFacade;
import id.code.akademik.filter.TahunPelajaranFilter;
import id.code.akademik.model.TahunPelajaranModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class TahunPelajaranHandler extends RouteApiHandler<AccessTokenPayload> {
    private final TahunPelajaranFacade tahunPelajaranFacade = new TahunPelajaranFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<TahunPelajaranFilter> filter) throws Exception {
        final List<TahunPelajaranModel> items = this.tahunPelajaranFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{kdThn}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, String kdThn) throws Exception {
        final TahunPelajaranModel data = this.tahunPelajaranFacade.findByKdThn(kdThn);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody TahunPelajaranModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.tahunPelajaranFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{kdThn}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, String kdThn) throws Exception {
        final boolean deleted = this.tahunPelajaranFacade.delete(kdThn);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{kdThn}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody TahunPelajaranModel newData, String kdThn) throws Exception {
        newData.setKdThn(kdThn);

        if (this.tahunPelajaranFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}