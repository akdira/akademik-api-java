package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.AbsensiHarianDetailFacade;
import id.code.akademik.filter.AbsensiHarianDetailFilter;
import id.code.akademik.model.AbsensiHarianDetailModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class AbsensiHarianDetailHandler extends RouteApiHandler<AccessTokenPayload> {
    private final AbsensiHarianDetailFacade absensiHarianDetailFacade = new AbsensiHarianDetailFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<AbsensiHarianDetailFilter> filter) throws Exception {
        final List<AbsensiHarianDetailModel> items = this.absensiHarianDetailFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{kdAbsenDetail}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, int kdAbsenDetail) throws Exception {
        final AbsensiHarianDetailModel data = this.absensiHarianDetailFacade.findByKdAbsenDetail(kdAbsenDetail);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody AbsensiHarianDetailModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.absensiHarianDetailFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{kdAbsenDetail}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, int kdAbsenDetail) throws Exception {
        final boolean deleted = this.absensiHarianDetailFacade.delete(kdAbsenDetail);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{kdAbsenDetail}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody AbsensiHarianDetailModel newData, Integer kdAbsenDetail) throws Exception {
        newData.setKdAbsenDetail(kdAbsenDetail);

        if (this.absensiHarianDetailFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}