package id.code.akademik.api.handler;

import id.code.database.filter.Filter;
import id.code.server.route.RouteApiHandler;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.RequestBody;
import id.code.server.ServerExchange;
import id.code.server.ApiResponse;

import id.code.akademik.api.AccessTokenPayload;
import id.code.akademik.facade.WaliKelasFacade;
import id.code.akademik.filter.WaliKelasFilter;
import id.code.akademik.model.WaliKelasModel;

import java.util.List;

import static id.code.server.ApiHttpStatus.*;

/**
 * Created by Web Api Generator 19/07/2018.
 */

public class WaliKelasHandler extends RouteApiHandler<AccessTokenPayload> {
    private final WaliKelasFacade waliKelasFacade = new WaliKelasFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<WaliKelasFilter> filter) throws Exception {
        final List<WaliKelasModel> items = this.waliKelasFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{kdWk}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, long kdWk) throws Exception {
        final WaliKelasModel data = this.waliKelasFacade.findByKdWk(kdWk);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody WaliKelasModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.waliKelasFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{kdWk}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, long kdWk) throws Exception {
        final boolean deleted = this.waliKelasFacade.delete(kdWk);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{kdWk}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody WaliKelasModel newData, Long kdWk) throws Exception {
        newData.setKdWk(kdWk);

        if (this.waliKelasFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}