package id.code.akademik.api;

import id.code.component.utility.StringUtility;
import id.code.server.ApiClaim;
import id.code.akademik.model.UserLoginModel;

import java.util.Map;


/**
 * Created by Web Api Generator 19/07/2018.
 */

public class AccessTokenPayload extends ApiClaim {
    private int idUser;

    public AccessTokenPayload(UserLoginModel user) {
        super(String.valueOf(user.getIdUser()), null);
        this.idUser = user.getIdUser();
    }

    public AccessTokenPayload(Map payload) {
        super(payload);
        this.idUser = StringUtility.getInt(super.getClaimId());
    }

    @Override
    public boolean isEmpty() { return this.idUser == 0; }
    public int getIdUser() { return this.idUser; }

    @Override
    public Map toMap() {
        final Map map = super.toMap();
        map.put(UserLoginModel._ID_USER, this.idUser);
        return map;
    }
}